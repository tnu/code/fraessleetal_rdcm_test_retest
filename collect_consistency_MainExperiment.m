function collect_consistency_MainExperiment(variant,task,acq,parcellation_type,model)
% Collects the group-level consistency results for the effective connectivity 
% parameters from the whole-brain Dynamic Causal Models (DCMs) for the 
% resting-state and task-based dataset of HCP.
%
% Input:
%   task                -- which task is analyzed
%   acq                 -- which acquisition is analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
%   model               -- model to be analyzed
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% folder for different rDCM variants
if ( variant == 1 )
	rdcm_folder = 'regressionDCM';
elseif ( variant == 2 )
    rdcm_folder = 'sparse_regressionDCM';
end

% functional connectivity variants
fc_variant_name = {'pearsonCorr','partialCorr'};


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

% task names for title
task_title = {'REST','EMOTION','GAMBLING','LANGUAGE','MOTOR','RELATIONAL',...
    'SOCIAL','WM'};


% which tasks to analyze
if ( isempty(task) )
    task_analyze = 1:length(task_name);
else
    task_analyze = task;
end

% which tasks to analyze
if ( isempty(parcellation_type) )
    parcellation_type_analyze = 1:length(parcellations);
else
    parcellation_type_analyze = parcellation_type;
end


% correlation arrays
R_Amatrix_allSubjects 	= NaN(length(parcellations),length(task_title));
p_Amatrix_allSubjects   = NaN(length(parcellations),length(task_title));
R_Cmatrix_allSubjects   = NaN(length(parcellations),length(task_title));
p_Cmatrix_allSubjects   = NaN(length(parcellations),length(task_title));
R_FCmatrix_allSubjects 	= NaN(length(parcellations),length(task_title));
p_FCmatrix_allSubjects	= NaN(length(parcellations),length(task_title));


% iterate over parcellation schemes
for parcellation_type = parcellation_type_analyze
    
    % output the parcellation scheme
    disp(parcellations{parcellation_type})

    % iterate over tasks
    for task = task_analyze

        % set the SPM path and the path to the experiment
        foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},rdcm_folder);

        % search for the filename
        if ( isempty(acq) )
            filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
            filename = filename(1).name;
        else
            filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
            filename = filename(acq).name;
        end


        % load the results array
        temp    = load(fullfile(foldername,filename));
        results = temp.results;


        % asign the data
        A_matrix_allSubjects     = results.A_Matrix_AllSubjects;
        C_matrix_allSubjects     = results.C_Matrix_AllSubjects;


        % initialize the temporary arrays
        mean_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));
        

        % get endogenous parameters
        for int = 1:size(A_matrix_allSubjects,1)
            for int2 = 1:size(A_matrix_allSubjects,2)

                % subjects with data
                subject_vector = find(isfinite(A_matrix_allSubjects{int,int2,1}) & isfinite(A_matrix_allSubjects{int,int2,2}));

                % get the mean endogenous connection strength
                for int3 = 1:size(A_matrix_allSubjects,3)
                    mean_Amatrix_allSubjects(int,int2,int3) = mean(A_matrix_allSubjects{int,int2,int3}(subject_vector));
                end
            end
        end


        % initialize the temporary arrays
        mean_Cmatrix_allSubjects = NaN(size(C_matrix_allSubjects));
        
        
        % for task-based fMRI
        if ( task > 1 )

            % get driving input parameters
            for int = 1:size(C_matrix_allSubjects,1)
                for int2 = 1:size(C_matrix_allSubjects,2)

                    % subjects with data
                    subject_vector = find(isfinite(C_matrix_allSubjects{int,int2,1}) & isfinite(C_matrix_allSubjects{int,int2,2}));

                    % get the mean endogenous connection strength
                    for int3 = 1:size(C_matrix_allSubjects,3)
                        mean_Cmatrix_allSubjects(int,int2,int3) = mean(C_matrix_allSubjects{int,int2,int3}(subject_vector));
                    end
                end
            end
        end
        
        % get the group-average connectivity estiamtes
        if ( variant == 1 )
            mean_S1 = mean_Amatrix_allSubjects(:,:,1); mean_S1 = mean_S1 - diag(diag(mean_S1)); mean_S1 = mean_S1(mean_S1~=0);
            mean_S2 = mean_Amatrix_allSubjects(:,:,2); mean_S2 = mean_S2 - diag(diag(mean_S2)); mean_S2 = mean_S2(mean_S2~=0);
        elseif ( variant == 2 )
            diag_part = eye(size(mean_Amatrix_allSubjects,1));
            mean_S1 = mean_Amatrix_allSubjects(:,:,1); mean_S1 = mean_S1(diag_part~=1);
            mean_S2 = mean_Amatrix_allSubjects(:,:,2); mean_S2 = mean_S2(diag_part~=1);
        end
        
        
        % get the correlation of the group-average connectivity estimates
        [R_temp, p_temp] = corrcoef(mean_S1,mean_S2);

        
        % asign the correlation coefficient
        R_Amatrix_allSubjects(parcellation_type,task) = R_temp(2,1);
        p_Amatrix_allSubjects(parcellation_type,task) = p_temp(2,1);
        

        % investigate the driving inputs
        if ( task > 1 )
            
            % get the group-average connectivity estiamtes
            mean_S1 = mean_Cmatrix_allSubjects(:,:,1); mean_S1 = mean_S1(mean_S1~=0);
            mean_S2 = mean_Cmatrix_allSubjects(:,:,2); mean_S2 = mean_S2(mean_S2~=0);

            % get the correlation of the group-average connectivity estimates
            [R_temp, p_temp] = corrcoef(mean_S1,mean_S2);
            
            % asign the correlation coefficient
            R_Cmatrix_allSubjects(parcellation_type,task) = R_temp(2,1);
            p_Cmatrix_allSubjects(parcellation_type,task) = p_temp(2,1);
            
        end
        
        
        % set the SPM path and the path to the experiment
        foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_fc',parcellations{parcellation_type},fc_variant_name{variant});

        % search for the filename
        if ( isempty(acq) )
            filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
            filename = filename(1).name;
        else
            filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername,[task_name{task} '2*']))];
            filename = filename(acq).name;
        end


        % load the results array
        temp    = load(fullfile(foldername,filename));
        results = temp.results;


        % asign the data
        if ( variant == 1 )
            FC_matrix_allSubjects	= results.fullCorr_AllSubjects;
        elseif ( variant == 2 )
            FC_matrix_allSubjects	= results.partialCorrReg_AllSubjects;
        end
        

        % initialize the temporary arrays
        mean_FCmatrix_allSubjects = NaN(size(FC_matrix_allSubjects));


        % get endogenous parameters
        for int = 1:size(FC_matrix_allSubjects,1)
            for int2 = 1:size(FC_matrix_allSubjects,2)

                % subjects with data
                subject_vector = find(isfinite(FC_matrix_allSubjects{int,int2,1}) & isfinite(FC_matrix_allSubjects{int,int2,2}));
                
                % iterate over sessions
                for int3 = 1:size(FC_matrix_allSubjects,3)
                    mean_FCmatrix_allSubjects(int,int2,int3) = tanh(mean(atanh(FC_matrix_allSubjects{int,int2,int3}(subject_vector))));
                end
            end
        end
        
        % get the group-average connectivity estiamtes
        mean_S1 = mean_FCmatrix_allSubjects(:,:,1); mean_S1 = mean_S1 - diag(diag(mean_S1)); mean_S1 = mean_S1(mean_S1~=0);
        mean_S2 = mean_FCmatrix_allSubjects(:,:,2); mean_S2 = mean_S2 - diag(diag(mean_S2)); mean_S2 = mean_S2(mean_S2~=0);

        % get the correlation of the group-average connectivity estimates
        [R_temp, p_temp] = corrcoef(mean_S1,mean_S2);

        
        % asign the correlation coefficient
        R_FCmatrix_allSubjects(parcellation_type,task) = R_temp(2,1);
        p_FCmatrix_allSubjects(parcellation_type,task) = p_temp(2,1);
        
        
        % compare the correlation coefficients
        za      = 0.5*log((1+R_Amatrix_allSubjects(parcellation_type,task))/(1-R_Amatrix_allSubjects(parcellation_type,task)));
        zb      = 0.5*log((1+R_FCmatrix_allSubjects(parcellation_type,task))/(1-R_FCmatrix_allSubjects(parcellation_type,task)));
        szab    = sqrt(1/(length(mean_S1)-3) + 1/(length(mean_S1)-3));
        z       = abs(za-zb)/szab;
        p_comp  = 2*(1-normcdf(z, 0, 1));
        
        
        % print the correlation coefficient
        fprintf('\t %25s R(A) = %1.2f, p(A) = %1.3f | R(C) = %1.2f, p(C) = %1.3f | R(FC) = %1.2f, p(FC) = %1.3f || p(A-FC) = %1.3f \n',[task_title{task} ':'],...
            round(R_Amatrix_allSubjects(parcellation_type,task),2),round(p_Amatrix_allSubjects(parcellation_type,task),3),...
            round(R_Cmatrix_allSubjects(parcellation_type,task),2),round(p_Cmatrix_allSubjects(parcellation_type,task),3),...
            round(R_FCmatrix_allSubjects(parcellation_type,task),2),round(p_FCmatrix_allSubjects(parcellation_type,task),3),...
            round(p_comp,3))
        
    end
end

end