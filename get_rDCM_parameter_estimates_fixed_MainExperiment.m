function get_rDCM_parameter_estimates_fixed_MainExperiment(task,acq,model,parcellation_type)
% Get the individual parameter estimates from the whole-brain Dynamic Causal 
% Models (DCMs) for the resting state dataset of the HCP dataset.
% Parameter estimates have been computed using regression DCM (rDCM).
% 
% This function reads the DCM files that have been estimated using
% regression DCM. The function stores the individual parameter estimates
% for the sparse effective connectivity patterns.
% 
% Input:
%   model               -- model to analyze
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% session name
sesison_name = {'test','retest'};


% asign the foldernames
foldername = FilenameInfo.Data_Path;


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*'));...
    dir(fullfile(foldername,'3*')); dir(fullfile(foldername,'4*'));...
    dir(fullfile(foldername,'5*')); dir(fullfile(foldername,'6*'));...
    dir(fullfile(foldername,'7*')); dir(fullfile(foldername,'8*'));...
    dir(fullfile(foldername,'9*'))];


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

    
% iterate over all subjects
for id = 1:length(Subject_List)

    % subject ID
    ID = Subject_List(id).name;
        
    % iterative over sessions
    for session = 1:length(sesison_name)

        % foldername
        foldername_rDCM = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},'firstlevel_dcm',parcellations{parcellation_type},'regressionDCM');
        
        % search for the filename
        if ( isempty(acq) )
            filename    = dir(fullfile(foldername_rDCM,[task_name{task} '_comb_*']));
            file_select = 1;
        else
            filename    = [dir(fullfile(foldername_rDCM,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
            file_select = acq;
        end
        
        % get the data
        if ( ~isempty(filename) )
            
            % select the filename
            filename = filename(file_select).name;
            
            try
            
                % load the DCM
                temp           = load(fullfile(foldername_rDCM,filename));
                output         = temp.output;
                
                % display subject name
                fprintf(['Subject: ' ID ' (' sesison_name{session} ' - ' num2str(id) ')\t - found \n'])
                
            catch
                
                % load a dummy result to get network size
                foldername_dummy = fullfile(foldername,'103818','MNINonLinear','Results',sesison_name{session},'firstlevel_dcm',parcellations{parcellation_type},'regressionDCM');
                file_dummy = dir(fullfile(foldername_dummy, filename));
                temp = load(fullfile(foldername_dummy,file_dummy(1).name));

                % define dummy connectivity and driving input matrices
                output.Ep.A = NaN(size(temp.output.Ep.A,1),size(temp.output.Ep.A,2));
                output.Ep.C = NaN(size(temp.output.Ep.C,1),size(temp.output.Ep.C,2));
                
                % define dummy connectivity and driving input variance matrices
                output.Vp.A = NaN(size(temp.output.Vp.A,1),size(temp.output.Vp.A,2));
                if ( task > 1 )
                    output.Vp.C = NaN(size(temp.output.Vp.C,1),size(temp.output.Vp.C,2));
                end
                
                % define a dummy array
                output.logF = NaN;
                
                % display subject name
                fprintf(['Subject: ' ID ' (' sesison_name{session} ' - ' num2str(id) ')\t - missing (wrong) \n'])
                
            end

            % clear the DCM file
            clear temp

        else

            % display subject name
            fprintf(['Subject: ' ID ' (' sesison_name{session} ' - ' num2str(id) ')\t - missing \n'])

            % load a dummy result to get network size
            foldername_dummy = fullfile(foldername,'103818','MNINonLinear','Results',sesison_name{session},'firstlevel_dcm',parcellations{parcellation_type},'regressionDCM');
            
            % search for the filename
            if ( isempty(acq) )
                filename = dir(fullfile(foldername_dummy,[task_name{task} '_comb_*']));
                filename = filename(1).name;
            else
                filename = [dir(fullfile(foldername_dummy,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
                filename = filename(acq).name;
            end
            
            file_dummy = dir(fullfile(foldername_dummy, filename));
            temp = load(fullfile(foldername_dummy,file_dummy(1).name));

            % define dummy connectivity and driving input matrices
            output.Ep.A = NaN(size(temp.output.Ep.A,1),size(temp.output.Ep.A,2));
            output.Ep.C = NaN(size(temp.output.Ep.C,1),size(temp.output.Ep.C,2));
            
            % define dummy connectivity and driving input variance matrices
            output.Vp.A = NaN(size(temp.output.Vp.A,1),size(temp.output.Vp.A,2));
            if ( task > 1 )
                output.Vp.C = NaN(size(temp.output.Vp.C,1),size(temp.output.Vp.C,2));
            end

            % define a dummy array
            output.logF = NaN;

            % clear the DCM file
            clear temp

        end

        % define cells
        if ( id == 1 && session == 1 )
            A_Matrix_AllSubjects    = cell(size(output.Ep.A,1),size(output.Ep.A,2),length(sesison_name));
            C_Matrix_AllSubjects    = cell(size(output.Ep.C,1),size(output.Ep.C,2),length(sesison_name));
            vA_Matrix_AllSubjects   = cell(size(output.Vp.A,1),size(output.Vp.A,2),length(sesison_name));
            if ( task > 1 )
                vC_Matrix_AllSubjects  	= cell(size(output.Vp.C,1),size(output.Vp.C,2),length(sesison_name));
            end
            F_AllSubjects         	= cell(length(sesison_name),1);
        end

        % asign the values for the endogenous parameters in each subject
        for int = 1:size(output.Ep.A,1)
            for int2 = 1:size(output.Ep.A,2)
                A_Matrix_AllSubjects{int,int2,session} = [A_Matrix_AllSubjects{int,int2,session}; output.Ep.A(int,int2)];
            end 
        end

        % asign the values for the driving parameters in each subject
        for int = 1:size(output.Ep.C,1)
            for int2 = 1:size(output.Ep.C,2)
                C_Matrix_AllSubjects{int,int2,session} = [C_Matrix_AllSubjects{int,int2,session}; output.Ep.C(int,int2)];
            end 
        end
        
        % asign the variances for the endogenous parameters in each subject
        for int = 1:size(output.Ep.A,1)
            for int2 = 1:size(output.Ep.A,2)
                vA_Matrix_AllSubjects{int,int2,session} = [vA_Matrix_AllSubjects{int,int2,session}; output.Vp.A(int,int2)];
            end 
        end

        % asign the variances for the driving parameters in each subject
        if ( task > 1 )
            for int = 1:size(output.Ep.C,1)
                for int2 = 1:size(output.Ep.C,2)
                    vC_Matrix_AllSubjects{int,int2,session} = [vC_Matrix_AllSubjects{int,int2,session}; output.Vp.C(int,int2)];
                end 
            end
        end
        
        
        % asign the negative free energy
        F_AllSubjects{session,1}           = [F_AllSubjects{session,1}; output.logF];


        % asign the subject name
        if ( id == 1 && session == 1 )
            results.AllSubjects{id}  	= ID;
        else
            results.AllSubjects{end+1}  = ID;
        end

%         % asign the region names
%         if ( site == 1 && subject == 1 )
%             results.AllRegions        = output.signal.name;
%         end
    end
end
    
% asign the results
results.A_Matrix_AllSubjects               = A_Matrix_AllSubjects;
results.C_Matrix_AllSubjects               = C_Matrix_AllSubjects;
results.vA_Matrix_AllSubjects              = vA_Matrix_AllSubjects;
if ( task > 1 )
    results.vC_Matrix_AllSubjects          = vC_Matrix_AllSubjects;
end
results.F_AllSubjects                      = F_AllSubjects;


% create the results folder
if ( ~exist(fullfile(foldername,'grouplevel_dcm',parcellations{parcellation_type},'regressionDCM'),'dir') )
    mkdir(fullfile(foldername,'grouplevel_dcm',parcellations{parcellation_type},'regressionDCM'))
end

% save the estimated result
if ( ~isempty(results) )
    save(fullfile(foldername,'grouplevel_dcm',parcellations{parcellation_type},'regressionDCM', filename), 'results', '-v7.3')
end

end