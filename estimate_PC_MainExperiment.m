function estimate_PC_MainExperiment(id,task,acq_combine,session,parcellation_type)
% Infer whole-brain functional connectivity for the resting-state dataset of
% the HCP dataset using L1-regularized partial correlation.
% 
% Input:
%   id                 	-- subject to analyze
%   task                -- HCP tasks
%   acq_combine         -- combine or don't combine within-session scans
%   session             -- HCP session: (1) test, (2) retest
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add the rDCM toolbox
addpath(genpath(fullfile(FilenameInfo.TAPAS_Path,'rDCM')))
addpath(FilenameInfo.SPM_Path)
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))
addpath(fullfile(FilenameInfo.Matlab_Path,'L1precision'))


% initialize spm
spm_jobman('initcfg')


% set the parcellation
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% session name
sesison_name     = {'test','retest'};


% asign the foldernames
foldername = FilenameInfo.Data_Path;


% get the subject list
Subject_List = dir(fullfile(foldername,'*'));
Subject_List = Subject_List(3:end);


% choose which subjects to analyze
if ( isempty(id) )
    id_analyze = 1:length(Subject_List);
else
    id_analyze = id;
end

% choose which session to analyze
if ( isempty(id) )
    session_analyze = 1:length(sesison_name);
else
    session_analyze = session;
end

% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};


% iterate overs subjects
for id = id_analyze
    
    % subject ID
    ID = Subject_List(id).name;
    
    % iterate over sessions
    for session = session_analyze
        
        % print progress
        fprintf(['\nProcessing: ' ID ' (' sesison_name{session} ')\n'])
        
        
        % clear relevant files
        clear DCM 
        clear output 
        clear options


        % define the standard option settings
        options.scale      = 1;
        options.estimateVL = 0;
        
        
        % display progress
        disp('Loading data...')
        
        
        % all folders
        aquisition_folder_all = dir(fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},[task_name{task} '*']));
        
        
        % which acquisitions to combine
        if ( isempty(acq_combine) )
            acq_combine_all = 1:length(aquisition_folder_all);
        else
            acq_combine_all = acq_combine;
        end
        
        
        % combine LR and RL acquisition or use only one
        for acquisition = acq_combine_all
    
            % foldername
            foldername_id = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},aquisition_folder_all(acquisition).name,parcellations{parcellation_type});
            
            % filename
            if ( task < 2 )
                filename = [aquisition_folder_all(acquisition).name '_Atlas_hp2000_clean'];
            else
                filename = [aquisition_folder_all(acquisition).name '_Atlas'];
            end
            
            
            % get the time series
            temp = ft_read_cifti(fullfile(foldername_id,[filename '.ptseries.nii']),'mapname','array');

            % specify the TR
            DCM.Y.dt  = temp.time(2)-temp.time(1);

            % specify the Y component of the DCM file
            DCM.Y.X0 = [];
            
            % specify empty cell array
            name = cell(1,size(temp.ptseries,1));
            
            % asign the data to the Y structure
            for i = 1:size(temp.ptseries,1)
                Y(:,i)  = temp.ptseries(i,:)';
                name{i} = temp.label{i};
            end
            
            % asign the data to the DCM.Y structure
            if ( acquisition == 1 )
                DCM.Y.y    = Y;
                DCM.Y.name = name;
            else
                DCM.Y.y    = [DCM.Y.y; Y];
            end
        end
            
            
        % diplay progress
        disp(['Found number of regions: ' num2str(size(DCM.Y.y,2))])
        disp(['Found number of scans (per region): ' num2str(size(DCM.Y.y,1))])
        
        
        % number of regions
        DCM.n = size(DCM.Y.y,2);

        % number of time points
        DCM.v = size(DCM.Y.y,1);
        
        
        % dcm folder
        fc_folder = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},'firstlevel_fc',parcellations{parcellation_type});
        
        % check whether the folder exists
        if ( ~exist(fc_folder,'dir') )
            mkdir(fc_folder)
        end


        % detrend and scale the data
        if ( options.scale )

            % detrend data
            DCM.Y.y = spm_detrend(DCM.Y.y);

            % scale data
            scale_factor   = max(max((DCM.Y.y))) - min(min((DCM.Y.y)));
            scale_factor   = 4/max(scale_factor,4);
            DCM.Y.y        = DCM.Y.y*scale_factor;

        end
        

        % create the directory
        if ( ~exist(fullfile(fc_folder, 'partialCorr'),'dir') )
            mkdir(fullfile(fc_folder, 'partialCorr'))
        end


        % display the progress
        disp(['Evaluating subject ' ID])
        disp(' ')
        
        
        % evaluate partial correlation
        disp('Regularized partial correlation...')
        sigma             = DCM.Y.y' * DCM.Y.y;
        precisionReg      = L1precisionBCD(sigma,1);
        D                 = diag(sqrt(diag(precisionReg)));
        DInv              = inv(D);
        partialCorrReg    = -1 * DInv * precisionReg * DInv;
        
        % run the rDCM analysis
        output.partialCorrReg = partialCorrReg;
        
        
        % check if only one session or multiple sessions
        if ( length(acq_combine_all) ~= 1)
            if ( task < 2 )
                filename = [task_name{task} '_comb_Atlas_hp2000_clean'];
            else
                filename = [task_name{task} '_comb_Atlas'];
            end
        end
        
        
        % save the estimated results
        if ( ~isempty(output) )
            save(fullfile(fc_folder, 'partialCorr', [filename '.mat']),'output')
        end
    
    end
end

end