function summarize_PC_MainExperiment(task,acq,parcellation_type)
% Evaluates the mean functional connectivity parameters from the whole-brain 
% functional connectivity analysis for the resting-state and task-based 
% dataset of HCP. Importantly, it also computes the intra-class correlation
% coefficient.
%
% Input:
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_fc',parcellations{parcellation_type},'partialCorr');


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};


% search for the filename
if ( isempty(acq) )
    filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
    filename = filename(1).name;
else
    filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
    filename = filename(acq).name;
end


% load the results array
temp    = load(fullfile(foldername,filename));
results = temp.results;


% asign the data
FC_matrix_allSubjects     = results.partialCorrReg_AllSubjects;


% initialize the temporary arrays
mean_FCmatrix_allSubjects = NaN(size(FC_matrix_allSubjects));
pVal_FCmatrix_allSubjects = NaN(size(FC_matrix_allSubjects));
ICC_FCmatrix_allSubjects  = NaN(size(FC_matrix_allSubjects,1),size(FC_matrix_allSubjects,2));
R_FCmatrix_allSubjects    = NaN(size(FC_matrix_allSubjects,1),size(FC_matrix_allSubjects,2));


% get endogenous parameters
for int = 1:size(FC_matrix_allSubjects,1)
    for int2 = 1:size(FC_matrix_allSubjects,2)
        
        % subjects with data
        subject_vector = find(isfinite(FC_matrix_allSubjects{int,int2,1}) & isfinite(FC_matrix_allSubjects{int,int2,2}));
        
        % display the number of subjects
        if ( int == 1 && int2 == 1)
            disp(['Found: ' num2str(length(subject_vector)) ' of ' num2str(length(FC_matrix_allSubjects{1,1,1})) ' subjects'])
        end
        
        % iterate over conditions
        for int3 = 1:size(FC_matrix_allSubjects,3)
            
            % get the mean endogenous connection strength
            mean_FCmatrix_allSubjects(int,int2,int3) = tanh(mean(atanh(FC_matrix_allSubjects{int,int2,int3}(subject_vector))));
            
            % asign the p value
            [~,p] = ttest(atanh(FC_matrix_allSubjects{int,int2,int3}(subject_vector)),0);
            pVal_FCmatrix_allSubjects(int,int2,int3) = p;
            
        end
        
        % compute the intra-class correlation coefficient and correlation
        r	= ICC([FC_matrix_allSubjects{int,int2,1}(subject_vector), FC_matrix_allSubjects{int,int2,2}(subject_vector)],'C-1');
        R   = corrcoef(FC_matrix_allSubjects{int,int2,1}(subject_vector), FC_matrix_allSubjects{int,int2,2}(subject_vector));
        
        
        % save ICC and R values
        ICC_FCmatrix_allSubjects(int,int2) = r;
        R_FCmatrix_allSubjects(int,int2)   = R(2,1);
        
    end
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},'partialCorr');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,[filename(1:end-4) '.pdf']),'file') )
        delete(fullfile(figure_folder,[filename(1:end-4) '.pdf']))
    end
end



% significance threshold (FDR-correction)
p_all               = pVal_FCmatrix_allSubjects(:);
p_all               = p_all(isfinite(p_all));
[~, crit_p, ~, ~]   = fdr_bh(p_all,0.05,'dep','no');
threshold           = crit_p+eps;

% choose all conections that are present
pVal_FCmatrix_allSubjects_select = pVal_FCmatrix_allSubjects < threshold; 


% define nice colormpa
[cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
cbrewer_colormap   = flipud(cbrewer_colormap);
cbrewer_colormap   = cbrewer_colormap([1:33 36 39:71],:);


% plot average endogenous connectivity
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_FCmatrix_allSubjects(:,:,1))
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-1 1])
set(gca,'xtick',[1 size(mean_FCmatrix_allSubjects,2)/2 size(mean_FCmatrix_allSubjects,2)])
set(gca,'ytick',[1 size(mean_FCmatrix_allSubjects,1)/2 size(mean_FCmatrix_allSubjects,1)])
title('Functional connectivity (Session 1) [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp1']), '-fillpage')


% plot average endogenous connectivity (significant)
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_FCmatrix_allSubjects(:,:,2))
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-1 1])
set(gca,'xtick',[1 size(mean_FCmatrix_allSubjects,2)/2 size(mean_FCmatrix_allSubjects,2)])
set(gca,'ytick',[1 size(mean_FCmatrix_allSubjects,1)/2 size(mean_FCmatrix_allSubjects,1)])
title('Functional connectivity (Session 2) [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp2']), '-fillpage')


% get the group-average connectivity estiamtes
mean_S1 = mean_FCmatrix_allSubjects(:,:,1); mean_S1 = mean_S1 - diag(diag(mean_S1)); mean_S1 = mean_S1(mean_S1~=0);
mean_S2 = mean_FCmatrix_allSubjects(:,:,2); mean_S2 = mean_S2 - diag(diag(mean_S2)); mean_S2 = mean_S2(mean_S2~=0);

% get the correlation of the group-average connectivity estimates
[R_temp, p_temp] = corrcoef(mean_S1,mean_S2);

% plot average endogenous connectivity (significant)
figure('units','normalized','outerposition',[0 0 1 1])
scatter(mean_S1, mean_S2, 50, [0.6 0.6 0.6], 'filled', 'MarkerEdgeColor', 'w')
hold on
plot([-1 1],[-1 1],'-','Color',cbrewer_colormap(65,:))
if ( p_temp(2,1) < 0.001 )
    text(0.5,-0.8,['r = ' num2str(round(R_temp(2,1),3)) ', p < 0.001'],'FontSize',12)
else
    text(0.5,-0.8,['r = ' num2str(round(R_temp(2,1),3)) ', p = ' num2str(round(p_temp(2,1),3))],'FontSize',12)
end
axis square
box off
xlim([-1 1])
ylim([-1 1])
xlabel('mean partial correlation (Session 1) [Hz]')
ylabel('mean partial correlation (Session 2) [Hz]')
title('Group-level consistency','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp3']), '-fillpage')



% get all non-zero and non-diagonal absolut values
temp = ICC_FCmatrix_allSubjects - diag(diag(ICC_FCmatrix_allSubjects));
ICC_FCmatrix_allSubjects_nodiag = temp(temp~=0);

% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
histogram(ICC_FCmatrix_allSubjects_nodiag,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
pd = fitdist(ICC_FCmatrix_allSubjects_nodiag,'Normal'); x = -1:0.01:1; y2 = pdf(pd,x);
hold on,
area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
ylimits = ylim();
text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_FCmatrix_allSubjects_nodiag),3))],'FontSize',12)
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (partial corr)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp4']), '-fillpage')



% get all non-zero and non-diagonal absolut values for significant connections
ICC_FCmatrix_allSubjects_sig       	= ICC_FCmatrix_allSubjects .* pVal_FCmatrix_allSubjects_select(:,:,1);
temp                                = ICC_FCmatrix_allSubjects_sig - diag(diag(ICC_FCmatrix_allSubjects_sig));
ICC_FCmatrix_allSubjects_nodiag_sig	= temp(temp~=0);

% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
histogram(ICC_FCmatrix_allSubjects_nodiag_sig,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
pd = fitdist(ICC_FCmatrix_allSubjects_nodiag_sig,'Normal'); x = -1:0.01:1; y2 = pdf(pd,x);
hold on,
area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
ylimits = ylim();
text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_FCmatrix_allSubjects_nodiag_sig),3))],'FontSize',12)
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (sig | partial corr)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp5']), '-fillpage')



% get all non-zero and non-diagonal absolut values
temp   = abs(mean_FCmatrix_allSubjects(:,:,1) - diag(diag(mean_FCmatrix_allSubjects(:,:,1))));
matrix = compute_top_connections_MainExperiment(temp,[],1000);

% get all non-zero and non-diagonal absolut values for top-1000 connections
ICC_FCmatrix_allSubjects_top      	= ICC_FCmatrix_allSubjects .* (matrix ~= 0);
temp                                = ICC_FCmatrix_allSubjects_top - diag(diag(ICC_FCmatrix_allSubjects_top));
ICC_FCmatrix_allSubjects_nodiag_top	= temp(temp~=0);

% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
histogram(ICC_FCmatrix_allSubjects_nodiag_top,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
pd = fitdist(ICC_FCmatrix_allSubjects_nodiag_top,'Normal'); x = -1:0.01:1; y2 = pdf(pd,x);
hold on,
area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
ylimits = ylim();
text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_FCmatrix_allSubjects_nodiag_top),3))],'FontSize',12)
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (top-1000 | partial corr)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp6']), '-fillpage')



% get all non-zero and non-diagonal absolut values
temp = mean_FCmatrix_allSubjects(:,:,1) - diag(diag(mean_FCmatrix_allSubjects(:,:,1)));
mean_FCmatrix_allSubjects_nodiag = temp(temp~=0);


% compute the correlation between connection strength and ICC
[R_temp,p_temp] = corrcoef(abs(mean_FCmatrix_allSubjects_nodiag), ICC_FCmatrix_allSubjects_nodiag);

% plot the correlation
figure('units','normalized','outerposition',[0 0 1 1])
scatter(abs(mean_FCmatrix_allSubjects_nodiag), ICC_FCmatrix_allSubjects_nodiag, 50, [0.6 0.6 0.6], 'filled', 'MarkerEdgeColor', 'w')
hold on
if ( p_temp(2,1) < 0.001 )
    text(0.015,-0.9,['r = ' num2str(round(R_temp(2,1),3)) ', p < 0.001'],'FontSize',12)
else
    text(0.015,-0.9,['r = ' num2str(round(R_temp(2,1),3)) ', p = ' num2str(round(p_temp(2,1),3))],'FontSize',12)
end
axis square
box off
xlim([0 1])
ylim([-1 1])
xlabel('mean parameter strength (partial corr)')
ylabel('intra-class correlation coefficient (ICC)')
title('Test-retest reliability (partial corr)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp7']), '-fillpage')


% append all PDFs
append_pdfs(fullfile(figure_folder,[filename(1:end-4) '.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp1.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp2.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp3.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp4.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp5.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp6.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp7.pdf']))


% delete the temporary files
delete(fullfile(figure_folder,[filename(1:end-4) '_temp*']))


% get the region names
temp_region = ft_read_cifti(fullfile(FilenameInfo.Data_Path,'103818','MNINonLinear','Results','test','rfMRI_REST1_LR',parcellations{parcellation_type},'rfMRI_REST1_LR_Atlas_hp2000_clean.ptseries.nii'),'mapname','array');

% asign the region names
AllRegions  = cell(size(temp_region.ptseries,1),1);
for i = 1:size(temp_region.ptseries,1)
    AllRegions{i} = temp_region.label{i};
end


% clear previous results structure
clear results

% collect the summary statistics results
results.mean_FCmatrix_allSubjects = mean_FCmatrix_allSubjects;
results.pVal_FCmatrix_allSubjects = pVal_FCmatrix_allSubjects;
results.AllRegions                = AllRegions;

% save the results
save(fullfile(foldername,['Summary_' filename]),'results');


% clear previous results structure
clear results

% collect the test-retest reliability results
results.ICC_FCmatrix_allSubjects = ICC_FCmatrix_allSubjects;
results.R_FCmatrix_allSubjects   = R_FCmatrix_allSubjects;


% foldername
foldernameICC = fullfile(FilenameInfo.Data_Path,'reliability',parcellations{parcellation_type},'partialCorr');

% create folder
if ( ~exist(foldernameICC,'dir') )
    mkdir(foldernameICC)
end

% save the results
save(fullfile(foldernameICC,filename),'results');

end