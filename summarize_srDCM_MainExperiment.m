function summarize_srDCM_MainExperiment(task,acq,parcellation_type,model,computeGT)
% Evaluates the mean effective connectivity parameters from the whole-brain 
% Dynamic Causal Models (DCMs) for the resting state dataset of the HCP 
% dataset. Here, connectivity estimates have been obtained utilizing sparsity
% constraints. Importantly, it also computes the intra-class correlation
% coefficient.
%
% Input:
%   task                -- which task is analyzed
%   acq                 -- which acquisition is analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
%   model               -- model to be analyzed
%   computeGT           -- compute graph theoretical measues
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},'sparse_regressionDCM');


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};


% search for the filename
if ( isempty(acq) )
    filename = dir(fullfile(foldername,[task_name{task} '_comb_*rDCM.mat']));
    filename = filename(1).name;
else
    filename = [dir(fullfile(foldername,[task_name{task} '1*rDCM.mat'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*rDCM.mat']))];
    filename = filename(acq).name;
end


% load the results array
temp    = load(fullfile(foldername,filename));
results = temp.results;


% asign the data
A_matrix_allSubjects     = results.A_Matrix_AllSubjects;
C_matrix_allSubjects     = results.C_Matrix_AllSubjects;

% asign the variances
vA_matrix_allSubjects     = results.vA_Matrix_AllSubjects;
if ( task > 1 )
    vC_matrix_allSubjects = results.vC_Matrix_AllSubjects;
end

% initialize the temporary arrays
mean_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));
pVal_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));
ICC_Amatrix_allSubjects  = NaN(size(A_matrix_allSubjects,1),size(A_matrix_allSubjects,2));
R_Amatrix_allSubjects    = NaN(size(A_matrix_allSubjects,1),size(A_matrix_allSubjects,2));
Pp_Amatrix_allSubjects   = NaN(size(A_matrix_allSubjects,1),size(A_matrix_allSubjects,2));



% get endogenous parameters
for int = 1:size(A_matrix_allSubjects,1)
    for int2 = 1:size(A_matrix_allSubjects,2)
        
        % subjects with data
        subject_vector = find(isfinite(A_matrix_allSubjects{int,int2,1}) & isfinite(A_matrix_allSubjects{int,int2,2}));
        
        % display the number of subjects
        if ( int == 1 && int2 == 1 )
            disp(['Found: ' num2str(length(subject_vector)) ' of ' num2str(length(A_matrix_allSubjects{1,1,1})) ' subjects'])
        end
        
        % iterate over conditions
        for int3 = 1:size(A_matrix_allSubjects,3)
            
            % get the mean endogenous connection strength
            mean_Amatrix_allSubjects(int,int2,int3) = mean(A_matrix_allSubjects{int,int2,int3}(subject_vector));
            
            % asign the p value
            [~,p] = ttest(A_matrix_allSubjects{int,int2,int3}(subject_vector),0);
            pVal_Amatrix_allSubjects(int,int2,int3) = p;
            
        end
        
        
        % compute the posterior probabilities
        for s = 1:length(subject_vector)
            if ( A_matrix_allSubjects{int,int2,1}(subject_vector(s)) < A_matrix_allSubjects{int,int2,2}(subject_vector(s)) )
                mu1 = A_matrix_allSubjects{int,int2,1}(subject_vector(s));
                mu2 = A_matrix_allSubjects{int,int2,2}(subject_vector(s));
                var1 = abs(vA_matrix_allSubjects{int,int2,1}(subject_vector(s)));
                var2 = abs(vA_matrix_allSubjects{int,int2,2}(subject_vector(s)));
            else
                mu1 = A_matrix_allSubjects{int,int2,2}(subject_vector(s));
                mu2 = A_matrix_allSubjects{int,int2,1}(subject_vector(s));
                var1 = abs(vA_matrix_allSubjects{int,int2,2}(subject_vector(s)));
                var2 = abs(vA_matrix_allSubjects{int,int2,1}(subject_vector(s)));
            end
            
            % compute the intersection point of the distributions
            c = (mu2*var1 - sqrt(var2)*(mu1*sqrt(var2)+sqrt(var1)*sqrt((mu1-mu2).^2 + 2*(var1-var2)*log(sqrt(var1)/sqrt(var2)))))/(var1-var2);

            % compute the area (probability of being equal)
            overlap = 1 - 0.5*erf((c-mu1)/(sqrt(2)*sqrt(var1))) + 0.5*erf((c-mu2)/(sqrt(2)*sqrt(var2)));
            
            % in case distributions are (numerically) identical
            if ( ~isfinite(overlap) )
                overlap = 1;
            end

            % probability of difference
            Pp(s) = 1 - overlap;
            
        end
        
        % get the probability
        Pp_Amatrix_allSubjects(int,int2) = mean(Pp);
        
        
        % compute the intra-class correlation coefficient and correlation
        r	= ICC([A_matrix_allSubjects{int,int2,1}(subject_vector), A_matrix_allSubjects{int,int2,2}(subject_vector)],'C-1');
        R   = corrcoef(A_matrix_allSubjects{int,int2,1}(subject_vector), A_matrix_allSubjects{int,int2,2}(subject_vector));
        
        % save ICC and R values
        if ( isfinite(r) )
            ICC_Amatrix_allSubjects(int,int2) = r;
            R_Amatrix_allSubjects(int,int2)   = R(2,1);
        else
            ICC_Amatrix_allSubjects(int,int2) = 0;
            R_Amatrix_allSubjects(int,int2)   = 0;
        end
    end
end


% initialize the temporary arrays
mean_Cmatrix_allSubjects = NaN(size(C_matrix_allSubjects));
pVal_Cmatrix_allSubjects = NaN(size(C_matrix_allSubjects));
ICC_Cmatrix_allSubjects  = NaN(size(C_matrix_allSubjects,1),size(C_matrix_allSubjects,2));
R_Cmatrix_allSubjects    = NaN(size(C_matrix_allSubjects,1),size(C_matrix_allSubjects,2));
Pp_Cmatrix_allSubjects   = NaN(size(C_matrix_allSubjects,1),size(C_matrix_allSubjects,2));


% for task-based fMRI
if ( task > 1 )

    % get driving input parameters
    for int = 1:size(C_matrix_allSubjects,1)
        for int2 = 1:size(C_matrix_allSubjects,2)

            % subjects with data
            subject_vector = find(isfinite(C_matrix_allSubjects{int,int2,1}) & isfinite(C_matrix_allSubjects{int,int2,2}));

            % iterate over conditions
            for int3 = 1:size(C_matrix_allSubjects,3)

                % get the mean endogenous connection strength
                mean_Cmatrix_allSubjects(int,int2,int3) = mean(C_matrix_allSubjects{int,int2,int3}(subject_vector));

                % asign the p value
                [~,p] = ttest(C_matrix_allSubjects{int,int2,int3}(subject_vector),0);
                pVal_Cmatrix_allSubjects(int,int2,int3) = p;

            end
            
            
            % compute the posterior probabilities
            for s = 1:length(subject_vector)
                if ( C_matrix_allSubjects{int,int2,1}(subject_vector(s)) < C_matrix_allSubjects{int,int2,2}(subject_vector(s)) )
                    mu1 = C_matrix_allSubjects{int,int2,1}(subject_vector(s));
                    mu2 = C_matrix_allSubjects{int,int2,2}(subject_vector(s));
                    var1 = vC_matrix_allSubjects{int,int2,1}(subject_vector(s));
                    var2 = vC_matrix_allSubjects{int,int2,2}(subject_vector(s));
                else
                    mu1 = C_matrix_allSubjects{int,int2,2}(subject_vector(s));
                    mu2 = C_matrix_allSubjects{int,int2,1}(subject_vector(s));
                    var1 = vC_matrix_allSubjects{int,int2,2}(subject_vector(s));
                    var2 = vC_matrix_allSubjects{int,int2,1}(subject_vector(s));
                end

                % compute the intersection point of the distributions
                c = (mu2*var1 - sqrt(var2)*(mu1*sqrt(var2)+sqrt(var1)*sqrt((mu1-mu2).^2 + 2*(var1-var2)*log(sqrt(var1)/sqrt(var2)))))/(var1-var2);

                % compute the area (probability of being equal)
                overlap = 1 - 0.5*erf((c-mu1)/(sqrt(2)*sqrt(var1))) + 0.5*erf((c-mu2)/(sqrt(2)*sqrt(var2)));
                
                % in case distributions are (numerically) identical
                if ( ~isfinite(overlap) )
                    overlap = 1;
                end
                
                % probability of difference
                Pp(s) = 1 - overlap;

            end

            % get the probability
            Pp_Cmatrix_allSubjects(int,int2) = mean(Pp);
            

            % compute the intra-class correlation coefficient and correlation
            r	= ICC([C_matrix_allSubjects{int,int2,1}(subject_vector), C_matrix_allSubjects{int,int2,2}(subject_vector)],'C-1');
            R   = corrcoef(C_matrix_allSubjects{int,int2,1}(subject_vector), C_matrix_allSubjects{int,int2,2}(subject_vector));


            % save ICC and R values
            ICC_Cmatrix_allSubjects(int,int2) = r;
            R_Cmatrix_allSubjects(int,int2)   = R(2,1);

        end
    end
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},'sparse_regressionDCM');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,[filename(1:end-4) '.pdf']),'file') )
        delete(fullfile(figure_folder,[filename(1:end-4) '.pdf']))
    end
end

if ( ~exist(fullfile(figure_folder,'RSN'),'dir') ) 
    mkdir(fullfile(figure_folder,'RSN'))
else
    if ( exist(fullfile(figure_folder,'RSN',[filename(1:end-4) '.pdf']),'file') )
        delete(fullfile(figure_folder,'RSN',[filename(1:end-4) '.pdf']))
    end
end



% significance threshold (FDR-correction)
p_all               = pVal_Amatrix_allSubjects(:);
p_all               = p_all(isfinite(p_all));
crit_p              = 0.05/length(p_all);
threshold           = crit_p+eps;

% choose all conections that are present
pVal_Amatrix_allSubjects_select = pVal_Amatrix_allSubjects(:,:,1) < threshold; 


% output the number of significant features
disp(['Found: # ' num2str(sum(pVal_Amatrix_allSubjects_select(:))) ' sig. features'])



% define nice colormpa
[cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
cbrewer_colormap   = flipud(cbrewer_colormap);
cbrewer_colormap   = cbrewer_colormap([1:33 36 39:71],:);

% get the maximum value
if ( task == 5 )
    c_limit = round_for_plot_MainExperiment(max(mean_Amatrix_allSubjects(:)),4);
elseif ( task == 8 )
    c_limit = round_for_plot_MainExperiment(max(mean_Amatrix_allSubjects(:)),5);
else
    c_limit = round_for_plot_MainExperiment(max(mean_Amatrix_allSubjects(:)),2);
end


% plot average endogenous connectivity
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_Amatrix_allSubjects(:,:,1))
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-1*c_limit c_limit])
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects,2)/2 size(mean_Amatrix_allSubjects,2)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects,1)/2 size(mean_Amatrix_allSubjects,1)])
title('Endogenous connectivity (Session 1) [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp1']), '-fillpage')


% plot average endogenous connectivity (significant)
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_Amatrix_allSubjects(:,:,2))
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-1*c_limit c_limit])
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects,2)/2 size(mean_Amatrix_allSubjects,2)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects,1)/2 size(mean_Amatrix_allSubjects,1)])
title('Endogenous connectivity (Session 2) [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp2']), '-fillpage')

% indices of diagonal entries
diag_part = eye(size(mean_Amatrix_allSubjects,1));

% get the group-average connectivity estiamtes
mean_S1 = mean_Amatrix_allSubjects(:,:,1); mean_S1 = mean_S1(diag_part~=1);
mean_S2 = mean_Amatrix_allSubjects(:,:,2); mean_S2 = mean_S2(diag_part~=1);

% get the correlation of the group-average connectivity estimates
[R_temp, p_temp] = corrcoef(mean_S1,mean_S2);

% plot average endogenous connectivity (significant)
figure('units','normalized','outerposition',[0 0 1 1])
scatter(mean_S1, mean_S2, 50, [0.6 0.6 0.6], 'filled', 'MarkerEdgeColor', 'w')
hold on
plot([-1*c_limit c_limit],[-1*c_limit c_limit],'-','Color',cbrewer_colormap(65,:))
if ( p_temp(2,1) < 0.001 )
    text(c_limit*0.8,-0.8*c_limit,['r = ' num2str(round(R_temp(2,1),3)) ', p < 0.001'],'FontSize',12)
else
    text(c_limit*0.8,-0.8*c_limit,['r = ' num2str(round(R_temp(2,1),3)) ', p = ' num2str(round(p_temp(2,1),3))],'FontSize',12)
end
axis square
box off
xlim([-1*c_limit c_limit])
ylim([-1*c_limit c_limit])
xlabel('mean parameter estimate (Session 1) [Hz]')
ylabel('mean parameter estimate (Session 2) [Hz]')
title('Group-level consistency (A-matrix)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp3']), '-fillpage')



% get all non-diagonal absolut values
temp                           = ICC_Amatrix_allSubjects - diag(diag(ICC_Amatrix_allSubjects));
ICC_Amatrix_allSubjects_nodiag = temp(temp~=0);


% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
histogram(ICC_Amatrix_allSubjects_nodiag,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
pd = fitdist(ICC_Amatrix_allSubjects_nodiag,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
hold on,
area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
ylimits = ylim();
text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_Amatrix_allSubjects_nodiag),3))],'FontSize',12)
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (A-matrix)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp4']), '-fillpage')



% get all non-zero and non-diagonal absolut values for significant connections
ICC_Amatrix_allSubjects_uncorr     	  = ICC_Amatrix_allSubjects .* ( pVal_Amatrix_allSubjects(:,:,1) < 0.001);
temp                                  = ICC_Amatrix_allSubjects_uncorr - diag(diag(ICC_Amatrix_allSubjects_uncorr));
ICC_Amatrix_allSubjects_nodiag_uncorr = temp(temp~=0);



% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
histogram(ICC_Amatrix_allSubjects_nodiag_uncorr,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
pd = fitdist(ICC_Amatrix_allSubjects_nodiag_uncorr,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
hold on,
area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
ylimits = ylim();
text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_Amatrix_allSubjects_nodiag_uncorr),3))],'FontSize',12)
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (sig uncorr | A-matrix)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp5']), '-fillpage')



% get all non-zero and non-diagonal absolut values for significant connections
ICC_Amatrix_allSubjects_sig         = ICC_Amatrix_allSubjects .* pVal_Amatrix_allSubjects_select(:,:,1);
temp                                = ICC_Amatrix_allSubjects_sig - diag(diag(ICC_Amatrix_allSubjects_sig));
ICC_Amatrix_allSubjects_nodiag_sig  = temp(temp~=0);


% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
if ( ~isempty(ICC_Amatrix_allSubjects_nodiag_sig) )
    histogram(ICC_Amatrix_allSubjects_nodiag_sig,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
    pd = fitdist(ICC_Amatrix_allSubjects_nodiag_sig,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
    hold on,
    area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
    ylimits = ylim();
    text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_Amatrix_allSubjects_nodiag_sig),3))],'FontSize',12)
end
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (sig | A-matrix)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp6']), '-fillpage')



% get all non-zero and non-diagonal absolut values
temp   = abs(mean_Amatrix_allSubjects(:,:,1) - diag(diag(mean_Amatrix_allSubjects(:,:,1))));
matrix = compute_top_connections_MainExperiment(temp,[],1000);

% get all non-zero and non-diagonal absolut values for top-1000 connections
ICC_Amatrix_allSubjects_nodiag_top = ICC_Amatrix_allSubjects(matrix~=0);


% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
histogram(ICC_Amatrix_allSubjects_nodiag_top,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
pd = fitdist(ICC_Amatrix_allSubjects_nodiag_top,'Kernel','Bandwidth',0.05); x = -1:0.01:1; y2 = pdf(pd,x);
hold on,
area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
ylimits = ylim();
text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_Amatrix_allSubjects_nodiag_top),3))],'FontSize',12)
axis square
box off
set(gca,'xtick',[0 0.5 1])
xlim([-1 1])
xlabel('intra-class correlation coefficient (ICC)')
ylabel('probability density function (PDF)')
title('Test-retest reliability (top-1000 | A-matrix)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp7']), '-fillpage')

% get all non-zero and non-diagonal absolut values
temp  = ICC_Amatrix_allSubjects - diag(diag(ICC_Amatrix_allSubjects));
temp2 = mean_Amatrix_allSubjects(:,:,1);
mean_Amatrix_allSubjects_nodiag = temp2(temp~=0);


% compute the correlation between connection strength and ICC
[R_temp,p_temp] = corrcoef(abs(mean_Amatrix_allSubjects_nodiag), ICC_Amatrix_allSubjects_nodiag);


% plot the correlation
figure('units','normalized','outerposition',[0 0 1 1])
scatter(abs(mean_Amatrix_allSubjects_nodiag), ICC_Amatrix_allSubjects_nodiag, 50, [0.6 0.6 0.6], 'filled', 'MarkerEdgeColor', 'w')
hold on
if ( p_temp(2,1) < 0.001 )
    text(c_limit*0.8,-0.9,['r = ' num2str(round(R_temp(2,1),3)) ', p < 0.001'],'FontSize',12)
else
    text(c_limit*0.8,-0.9,['r = ' num2str(round(R_temp(2,1),3)) ', p = ' num2str(round(p_temp(2,1),3))],'FontSize',12)
end
axis square
box off
xlim([0 c_limit])
ylim([-1 1])
xlabel('mean parameter estimate (rDCM) [Hz]')
ylabel('intra-class correlation coefficient (ICC)')
title('Test-retest reliability (A-Matrix)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp8']), '-fillpage')


% append all PDFs
append_pdfs(fullfile(figure_folder,[filename(1:end-4) '.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp1.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp2.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp3.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp4.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp5.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp6.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp7.pdf']),...
    fullfile(figure_folder,[filename(1:end-4) '_temp8.pdf']))


% delete the temporary files
delete(fullfile(figure_folder,[filename(1:end-4) '_temp*']))


% investigate the driving inputs
if ( task > 1 )
    
    % define nice colormap
    [cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
    cbrewer_colormap   = flipud(cbrewer_colormap);
    cbrewer_colormap   = cbrewer_colormap([1:33 36 39:71],:);

    % get the maximum value
    c_limit = round(max(mean_Cmatrix_allSubjects(:)),6);

    % plot average endogenous connectivity
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(mean_Cmatrix_allSubjects(:,:,1))
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis([-1*c_limit c_limit])
    if ( size(mean_Cmatrix_allSubjects,2) == 1 )
        set(gca,'xtick',1)
    elseif ( size(mean_Cmatrix_allSubjects,2) == 2 )
        set(gca,'xtick',[1 2])
    else
        set(gca,'xtick',[1 size(mean_Cmatrix_allSubjects,2)/2 size(mean_Cmatrix_allSubjects,2)])
    end
    set(gca,'ytick',[1 size(mean_Cmatrix_allSubjects,1)/2 size(mean_Cmatrix_allSubjects,1)])
    title('Driving inputs (Session 1) [Hz]','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp1']), '-fillpage')


    % plot average endogenous connectivity (significant)
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(mean_Cmatrix_allSubjects(:,:,2))
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis([-1*c_limit c_limit])
    if ( size(mean_Cmatrix_allSubjects,2) == 1 )
        set(gca,'xtick',1)
    elseif ( size(mean_Cmatrix_allSubjects,2) == 2 )
        set(gca,'xtick',[1 2])
    else
        set(gca,'xtick',[1 size(mean_Cmatrix_allSubjects,2)/2 size(mean_Cmatrix_allSubjects,2)])
    end
    set(gca,'ytick',[1 size(mean_Cmatrix_allSubjects,1)/2 size(mean_Cmatrix_allSubjects,1)])
    title('Driving inputs (Session 2) [Hz]','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp2']), '-fillpage')


    % get the group-average connectivity estiamtes
    mean_S1 = mean_Cmatrix_allSubjects(:,:,1);
    mean_S2 = mean_Cmatrix_allSubjects(:,:,2);

    % get the correlation of the group-average connectivity estimates
    [R_temp, p_temp] = corrcoef(mean_S1(:),mean_S2(:));

    % plot average endogenous connectivity (significant)
    figure('units','normalized','outerposition',[0 0 1 1])
    scatter(mean_S1(:), mean_S2(:), 50, [0.6 0.6 0.6], 'filled', 'MarkerEdgeColor', 'w')
    hold on
    plot([-1*c_limit c_limit],[-1*c_limit c_limit],'-','Color',cbrewer_colormap(65,:))
    if ( p_temp(2,1) < 0.001 )
        text(c_limit*0.8,-0.8*c_limit,['r = ' num2str(round(R_temp(2,1),3)) ', p < 0.001'],'FontSize',12)
    else
        text(c_limit*0.8,-0.8*c_limit,['r = ' num2str(round(R_temp(2,1),3)) ', p = ' num2str(round(p_temp(2,1),3))],'FontSize',12)
    end
    axis square
    box off
    xlim([-1*c_limit c_limit])
    ylim([-1*c_limit c_limit])
    xlabel('mean parameter estimate (Session 1) [Hz]')
    ylabel('mean parameter estimate (Session 2) [Hz]')
    title('Group-level consistency (C-Matrix)','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp3']), '-fillpage')



    % get all ICC values
    ICC_Cmatrix_allSubjects_vec = ICC_Cmatrix_allSubjects(:);

    % plot the distribution of edge weights
    figure('units','normalized','outerposition',[0 0 1 1]);
    histogram(ICC_Cmatrix_allSubjects_vec,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
    pd = fitdist(ICC_Cmatrix_allSubjects_vec,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
    hold on,
    area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
    ylimits = ylim();
    text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(ICC_Cmatrix_allSubjects_vec),3))],'FontSize',12)
    axis square
    box off
    set(gca,'xtick',[0 0.5 1])
    xlim([-1 1])
    xlabel('intra-class correlation coefficient (ICC)')
    ylabel('probability density function (PDF)')
    title('Test-retest reliability (C-Matrix)','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[filename(1:end-4) '_temp4']), '-fillpage')
    
    
    % compute the correlation between connection strength and ICC
    [R_temp,p_temp] = corrcoef(abs(mean_S1(:)), ICC_Cmatrix_allSubjects_vec);
    R_temp(1,2)
    p_temp(1,2)
    
    
    % append all PDFs
    append_pdfs(fullfile(figure_folder,[filename(1:end-4) '.pdf']),...
        fullfile(figure_folder,[filename(1:end-4) '_temp1.pdf']),...
        fullfile(figure_folder,[filename(1:end-4) '_temp2.pdf']),...
        fullfile(figure_folder,[filename(1:end-4) '_temp3.pdf']),...
        fullfile(figure_folder,[filename(1:end-4) '_temp4.pdf']))
    
    
    % delete the temporary files
    delete(fullfile(figure_folder,[filename(1:end-4) '_temp*']))
    
end


% % get the region names
% temp_region = ft_read_cifti(fullfile(FilenameInfo.Data_Path,'103818','MNINonLinear','Results','test','rfMRI_REST1_LR',parcellations{parcellation_type},'rfMRI_REST1_LR_Atlas_hp2000_clean.ptseries.nii'),'mapname','array');
% 
% % asign the region names
% AllRegions  = cell(size(temp_region.ptseries,1),1);
% for i = 1:size(temp_region.ptseries,1)
%     AllRegions{i} = temp_region.label{i};
% end

% clear previous results structure
clear results

% collect the summary statistics results
results.mean_Amatrix_allSubjects = mean_Amatrix_allSubjects;
results.mean_Cmatrix_allSubjects = mean_Cmatrix_allSubjects;
results.pVal_Amatrix_allSubjects = pVal_Amatrix_allSubjects;
results.pVal_Cmatrix_allSubjects = pVal_Cmatrix_allSubjects;
% results.AllRegions               = AllRegions;

% save the results
save(fullfile(foldername,['Summary_' filename]),'results');


% clear previous results structure
clear results

% collect the test-retest reliability results
results.ICC_Amatrix_allSubjects = ICC_Amatrix_allSubjects;
results.ICC_Cmatrix_allSubjects = ICC_Cmatrix_allSubjects;
results.R_Amatrix_allSubjects   = R_Amatrix_allSubjects;
results.R_Cmatrix_allSubjects   = R_Cmatrix_allSubjects;
results.Pp_Amatrix_allSubjects  = Pp_Amatrix_allSubjects;
results.Pp_Cmatrix_allSubjects  = Pp_Cmatrix_allSubjects;


% foldername
foldernameICC = fullfile(FilenameInfo.Data_Path,'reliability',parcellations{parcellation_type},'sparse_regressionDCM');

% create folder
if ( ~exist(foldernameICC,'dir') )
    mkdir(foldernameICC)
end

% save the results
save(fullfile(foldernameICC,filename),'results');


% % compute the within and between-module reliability
% if ( parcellation_type == 4 )
% 
%     % clear previous results structure
%     clear results
% 
%     % load the regions names
%     temp_regions = load(fullfile(foldername,['Summary_' filename]));
% 
%     % get the labels of the regions
%     labels = temp_regions.results.AllRegions;
% 
%     % name of modes from Yeo 7-Netowrks
%     Yeo7Network = {'Vis','SomMot','DorsAttn','VentAttn','Limbic','Cont','Default'};
% 
%     % empty asignment array
%     asignments_Yeo = zeros(length(labels),1);
% 
%     % get the assignments
%     for int = 1:length(Yeo7Network)
%         asignments_Yeo(contains(labels,Yeo7Network{int})) = int;
%     end
%     
%     % within RSN and between RSN
%     ICC_Amatrix_allSubjects_WM      = cell(length(Yeo7Network),1);
%     ICC_Amatrix_allSubjects_WM_sig  = cell(length(Yeo7Network),1);
%     
%     % indicator matrix for between RSN connections
%     bRSN_ind = ones(size(ICC_Amatrix_allSubjects));
%     
%     % iterate over regions
%     for int = 1:length(Yeo7Network)
%         
% 
%         % get all non-zero and non-diagonal absolut values within RSN
%         temp    = ICC_Amatrix_allSubjects - diag(diag(ICC_Amatrix_allSubjects));
%         temp_wm = temp(asignments_Yeo==int,asignments_Yeo==int);
%         ICC_Amatrix_allSubjects_WM{int} = temp_wm(temp_wm~=0);
%         
%         temp    = ICC_Amatrix_allSubjects_sig - diag(diag(ICC_Amatrix_allSubjects_sig));
%         temp_wm = temp(asignments_Yeo==int,asignments_Yeo==int);
%         ICC_Amatrix_allSubjects_WM_sig{int} = temp_wm(temp_wm~=0);
%         
%         % remove the within RSN connections
%         bRSN_ind(asignments_Yeo==int,asignments_Yeo==int) = 0;
%         
%     end
%     
%     % remove all within RSN connections
%     temp                           = ICC_Amatrix_allSubjects .* bRSN_ind;
%     ICC_Amatrix_allSubjects_BM_all = temp(temp~=0);
%     
%     temp                               = ICC_Amatrix_allSubjects_sig .* bRSN_ind;
%     ICC_Amatrix_allSubjects_BM_all_sig = temp(temp~=0);
%     
% 
%     % collate the within and between module ICCs
%     for int = 1:length(Yeo7Network)
%         if ( int == 1 )
%             ICC_Amatrix_allSubjects_WM_all      = ICC_Amatrix_allSubjects_WM{int};
%             ICC_Amatrix_allSubjects_WM_all_sig  = ICC_Amatrix_allSubjects_WM_sig{int};
%         else
%             ICC_Amatrix_allSubjects_WM_all      = [ICC_Amatrix_allSubjects_WM_all; ICC_Amatrix_allSubjects_WM{int}];
%             ICC_Amatrix_allSubjects_WM_all_sig  = [ICC_Amatrix_allSubjects_WM_all_sig; ICC_Amatrix_allSubjects_WM_sig{int}];
%         end
%     end
%     
%     % plot the distribution of edge weights
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     pd_WM = fitdist(ICC_Amatrix_allSubjects_WM_all,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_WM = pdf(pd_WM,x);
%     pd_BM = fitdist(ICC_Amatrix_allSubjects_BM_all,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_BM = pdf(pd_BM,x);
%     ha(2) = patch([y2_WM+2 y2_WM(1)+2]',[x 1]',[0.8 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
%     hold on
%     ha(1) = patch([y2_BM y2_BM(1)]',[x 1]',[0.4 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
%     xlimits = xlim();
%     plot([-0.5 xlimits(2)],[0 0],'k-')
%     axis square
%     box off
%     set(gca,'ytick',-1:0.5:1)
%     set(gca,'xtick',[])
%     legend(ha,{'between','within'},'Location','SE','FontSize',14)
%     legend boxoff
%     ylim([-1 1])
%     xlim([-0.5 xlimits(2)])
%     ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
%     xlabel('probability density function (PDF)','FontSize',14)
%     title('Within- and between-module connectivity','FontSize',18)
%     print(gcf, '-dpdf', fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp1']), '-fillpage')
%     
%     
%     % plot the distribution of edge weights
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     pd_WM = fitdist(ICC_Amatrix_allSubjects_WM_all_sig,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_WM = pdf(pd_WM,x);
%     pd_BM = fitdist(ICC_Amatrix_allSubjects_BM_all_sig,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_BM = pdf(pd_BM,x);
%     ha(2) = patch([y2_WM+2 y2_WM(1)+2]',[x 1]',[0.8 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
%     hold on
%     ha(1) = patch([y2_BM y2_BM(1)]',[x 1]',[0.4 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
%     xlimits = xlim();
%     plot([-0.5 xlimits(2)],[0 0],'k-')
%     axis square
%     box off
%     set(gca,'ytick',-1:0.5:1)
%     set(gca,'xtick',[])
%     legend(ha,{'between','within'},'Location','SE','FontSize',14)
%     legend boxoff
%     ylim([-1 1])
%     xlim([-0.5 xlimits(2)])
%     ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
%     xlabel('probability density function (PDF)','FontSize',14)
%     title('Within- and between-module connectivity (sig)','FontSize',18)
%     print(gcf, '-dpdf', fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp2']), '-fillpage')
%     
%     
%     
%     [cbrewer_colormap] = cbrewer('div', 'RdBu', 73, 'PCHIP');
%     cbrewer_colormap   = flipud(cbrewer_colormap);
%     cbrewer_colormap   = cbrewer_colormap([1:36 38:73],:);
%     
%     % plot the distribution of edge weights
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     for int = 1:length(Yeo7Network) 
%         if ( ~isempty(ICC_Amatrix_allSubjects_WM{int}) )
%             pd_RSN = fitdist(ICC_Amatrix_allSubjects_WM{int},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_RSN = pdf(pd_RSN,x);
%             ha(int) = patch([y2_RSN+3*(int-1) y2_RSN(1)+3*(int-1)]',[x 1]',cbrewer_colormap(1+10*(int-1),:),'FaceAlpha',0.4,'EdgeColor','none');
%             hold on
%         end
%         h = text(3*(int-1),-0.6,Yeo7Network{int},'FontSize',14);
%         set(h,'Rotation',270)
%     end
%     xlimits = xlim();
%     plot([-1 xlimits(2)],[0 0],'k-')
%     axis square
%     box off
%     set(gca,'ytick',-1:0.5:1)
%     set(gca,'xtick',[])
%     ylim([-1 1])
%     xlim([-0.5 xlimits(2)])
%     ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
%     xlabel('probability density function (PDF)','FontSize',14)
%     title('Resting-state Networks (RSN)','FontSize',18)
%     print(gcf, '-dpdf', fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp3']), '-fillpage')
%     
%     
%     % plot the distribution of edge weights
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     for int = 1:length(Yeo7Network)
%         if ( ~isempty(ICC_Amatrix_allSubjects_WM_sig{int}) )
%             pd_RSN = fitdist(ICC_Amatrix_allSubjects_WM_sig{int},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_RSN = pdf(pd_RSN,x);
%             ha(int) = patch([y2_RSN+3*(int-1) y2_RSN(1)+3*(int-1)]',[x 1]',cbrewer_colormap(1+10*(int-1),:),'FaceAlpha',0.4,'EdgeColor','none');
%             hold on
%         end
%         h = text(3*(int-1),-0.6,Yeo7Network{int},'FontSize',14);
%         set(h,'Rotation',270)
%     end
%     xlimits = xlim();
%     plot([-1 xlimits(2)],[0 0],'k-')
%     axis square
%     box off
%     set(gca,'ytick',-1:0.5:1)
%     set(gca,'xtick',[])
%     ylim([-1 1])
%     xlim([-0.5 xlimits(2)])
%     ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
%     xlabel('probability density function (PDF)','FontSize',14)
%     title('Resting-state Networks (sig | RSN)','FontSize',18)
%     print(gcf, '-dpdf', fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp4']), '-fillpage')
%     
%     
%     % append all PDFs
%     append_pdfs(fullfile(figure_folder,'RSN',[filename(1:end-4) '.pdf']),...
%         fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp1.pdf']),...
%         fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp2.pdf']),...
%         fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp3.pdf']),...
%         fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp4.pdf']))
%     
%     % delete the temporary files
%     delete(fullfile(figure_folder,'RSN',[filename(1:end-4) '_temp*']))
%     
% end
    

% graph theory analysis
if ( computeGT )

    % display the progress
    fprintf('\nComputing graph theoretical measures...\n')

    % clear results array
    clear results
    
    % iterate over session
    for session = 1:2
                
        % compute graph theoretical measures
        for s = 1:length(A_matrix_allSubjects{1,1,1})

            % get graph theoretical results or write empty array
            if ( any(s == subject_vector) )
            
                % display with subject is processed
                disp(['Processing: sub' num2str(s) ' - session: ' num2str(session)])

                % empty single subject matrix
                A_matrix_Subject = NaN(size(A_matrix_allSubjects,1),size(A_matrix_allSubjects,2));

                % create matrix from subject
                for int = 1:size(A_matrix_allSubjects,1)
                    for int2 = 1:size(A_matrix_allSubjects,2)
                        A_matrix_Subject(int,int2) = A_matrix_allSubjects{int,int2,session}(s);
                    end
                end

                % remove diagonal
                A_matrix_Subject = A_matrix_Subject - diag(diag(A_matrix_Subject));

                % transpose A-matrix (as Brain Connectivity toolbox uses switched notation)
                A_matrix_Subject = A_matrix_Subject';

                % compute graph theoretical measures
                if ( any(A_matrix_Subject(:)~=0) )
                    results_temp        = compute_graph_theoretical_measures(A_matrix_Subject);
                    results(s,session)  = results_temp;
                end
            
            else

                % display with subject is processed
                disp(['Processing: sub' num2str(s) ' - session: ' num2str(session) ' (excluded)'])

                % compute graph theoretical measures
                results_temp        = compute_graph_theoretical_measures([]);
                results(s,session)  = results_temp;

            end
        end
    end
    
    % store the graph theoretical measures
    save(fullfile(foldername,['GraphTheory_' filename]),'results');
    
end

end