function collect_correlation_rDCM_MainExperiment(mode,variant,task,acq,parcellation_type)
% Evaluates the correlation between mean effective connectivity parameters 
% or posterior precision from the whole-brain Dynamic Causal Models (DCMs)
% and test for correlations with the test-retest reliability. 
%
% Input:
%   mode                -- correlation with (1) connection strength, or (2) posterior precision
%   variant             -- which rDCM variant to use
%   task                -- which task is analyzed
%   acq                 -- which acquisition is analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};


% folder for different rDCM variants
if ( variant == 1 )
	rdcm_folder = 'regressionDCM';
elseif ( variant == 2 )
    rdcm_folder = 'sparse_regressionDCM';
end


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

% task names for title
task_title = {'REST','EMOTION','GAMBLING','LANGUAGE','MOTOR','RELATIONAL',...
    'SOCIAL','WM'};


% which tasks to analyze
if ( isempty(task) )
    task_analyze = 1:length(task_name);
else
    task_analyze = task;
end

% which tasks to analyze
if ( isempty(parcellation_type) )
    parcellation_type_analyze = 1:length(parcellations);
else
    parcellation_type_analyze = parcellation_type;
end


% correlation arrays
R_Amatrix_allSubjects 	= NaN(length(parcellations),length(task_title));
p_Amatrix_allSubjects   = NaN(length(parcellations),length(task_title));
R_Cmatrix_allSubjects   = NaN(length(parcellations),length(task_title));
p_Cmatrix_allSubjects   = NaN(length(parcellations),length(task_title));



% print which association is tested
if ( mode == 1 )
    disp('-------------')
    disp('Correlation with (average) connection strength')
    disp('-------------')
elseif ( mode == 2 )
    disp('-------------')
    disp('Correlation with (average) posterior precision')
    disp('-------------')
end


% iterate over parcellation schemes
for parcellation_type = parcellation_type_analyze
    
    % output the parcellation scheme
    disp(parcellations{parcellation_type})

    % iterate over tasks
    for task = task_analyze

        % set the SPM path and the path to the experiment
        foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},rdcm_folder);

        % search for the filename
        if ( isempty(acq) )
            filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
            filename = filename(1).name;
        else
            filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
            filename = filename(acq).name;
        end


        % load the results array
        temp    = load(fullfile(foldername,filename));
        results = temp.results;


        % asign the data
        A_matrix_allSubjects     = results.A_Matrix_AllSubjects;
        C_matrix_allSubjects     = results.C_Matrix_AllSubjects;
        
        % asign the variances
        vA_matrix_allSubjects     = results.vA_Matrix_AllSubjects;
        if ( task > 1 )
            vC_matrix_allSubjects = results.vC_Matrix_AllSubjects;
        end

        % initialize the temporary arrays
        mean_Amatrix_allSubjects    = NaN(size(A_matrix_allSubjects));
        mean_vAmatrix_allSubjects   = NaN(size(vA_matrix_allSubjects));
        pVal_Amatrix_allSubjects    = NaN(size(A_matrix_allSubjects));
        ICC_Amatrix_allSubjects     = NaN(size(A_matrix_allSubjects,1),size(A_matrix_allSubjects,2));

        
        % get endogenous parameters
        for int = 1:size(A_matrix_allSubjects,1)
            for int2 = 1:size(A_matrix_allSubjects,2)

                % subjects with data
                subject_vector = find(isfinite(A_matrix_allSubjects{int,int2,1}) & isfinite(A_matrix_allSubjects{int,int2,2}));

                % iterate over conditions
                for int3 = 1:size(A_matrix_allSubjects,3)

                    % get the mean endogenous connection strength
                    mean_Amatrix_allSubjects(int,int2,int3) = mean(A_matrix_allSubjects{int,int2,int3}(subject_vector));

                    % asign the p value
                    [~,p] = ttest(A_matrix_allSubjects{int,int2,int3}(subject_vector),0);
                    pVal_Amatrix_allSubjects(int,int2,int3) = p;
                    
                    % get the mean endogenous connection strength
                    mean_vAmatrix_allSubjects(int,int2,int3) = mean(vA_matrix_allSubjects{int,int2,int3}(subject_vector));

                end
                
                % compute the intra-class correlation coefficient
                r	= ICC([A_matrix_allSubjects{int,int2,1}(subject_vector), A_matrix_allSubjects{int,int2,2}(subject_vector)],'C-1');
                
                % save ICC values
                if ( isfinite(r) )
                    ICC_Amatrix_allSubjects(int,int2) = r;
                else
                    ICC_Amatrix_allSubjects(int,int2) = 0;
                end
                
            end
        end


        % initialize the temporary arrays
        mean_Cmatrix_allSubjects    = NaN(size(C_matrix_allSubjects));
        mean_vCmatrix_allSubjects   = NaN(size(C_matrix_allSubjects));
        pVal_Cmatrix_allSubjects    = NaN(size(C_matrix_allSubjects));
        ICC_Cmatrix_allSubjects     = NaN(size(C_matrix_allSubjects,1),size(C_matrix_allSubjects,2));


        % for task-based fMRI
        if ( task > 1 )

            % get driving input parameters
            for int = 1:size(C_matrix_allSubjects,1)
                for int2 = 1:size(C_matrix_allSubjects,2)

                    % subjects with data
                    subject_vector = find(isfinite(C_matrix_allSubjects{int,int2,1}) & isfinite(C_matrix_allSubjects{int,int2,2}));

                    % iterate over conditions
                    for int3 = 1:size(C_matrix_allSubjects,3)

                        % get the mean endogenous connection strength
                        mean_Cmatrix_allSubjects(int,int2,int3) = mean(C_matrix_allSubjects{int,int2,int3}(subject_vector));

                        % asign the p value
                        [~,p] = ttest(C_matrix_allSubjects{int,int2,int3}(subject_vector),0);
                        pVal_Cmatrix_allSubjects(int,int2,int3) = p;
                        
                        % get the mean endogenous connection strength
                        mean_vCmatrix_allSubjects(int,int2,int3) = mean(vC_matrix_allSubjects{int,int2,int3}(subject_vector));

                    end
                    
                    % compute the intra-class correlation coefficient
                    r	= ICC([C_matrix_allSubjects{int,int2,1}(subject_vector), C_matrix_allSubjects{int,int2,2}(subject_vector)],'C-1');
                    ICC_Cmatrix_allSubjects(int,int2) = r;
                    
                end
            end
        end


        % get all non-zero and non-diagonal absolut values
        temp = mean_Amatrix_allSubjects(:,:,1) - diag(diag(mean_Amatrix_allSubjects(:,:,1)));
        mean_Amatrix_allSubjects_nodiag = temp(temp~=0);
        
        
        % get all non-zero and non-diagonal absolut values
        if ( variant == 1 )
            temp = ICC_Amatrix_allSubjects - diag(diag(ICC_Amatrix_allSubjects));
            ICC_Amatrix_allSubjects_nodiag = temp(temp~=0);
        elseif ( variant == 2 )
            temp  = ICC_Amatrix_allSubjects - diag(diag(ICC_Amatrix_allSubjects));
            ICC_Amatrix_allSubjects_nodiag = temp(temp~=0);
            temp2 = mean_Amatrix_allSubjects(:,:,1);
            mean_Amatrix_allSubjects_nodiag = temp2(temp~=0);
        end
        
        % get all non-zero and non-diagonal precision values
        temp_precision = 1./mean_vAmatrix_allSubjects(:,:,1);
        mean_pAmatrix_allSubjects_nodiag = temp_precision(temp~=0);

        
        % compute the correlation between connection strength and ICC
        if ( mode == 1 )
            [R_temp,p_temp] = corrcoef(abs(mean_Amatrix_allSubjects_nodiag), ICC_Amatrix_allSubjects_nodiag);
        elseif ( mode == 2 )
            [R_temp,p_temp] = corrcoef(mean_pAmatrix_allSubjects_nodiag(isfinite(mean_pAmatrix_allSubjects_nodiag)), ICC_Amatrix_allSubjects_nodiag(isfinite(mean_pAmatrix_allSubjects_nodiag)));
        end

        % asign the correlation coefficient
        R_Amatrix_allSubjects(parcellation_type,task) = R_temp(2,1);
        p_Amatrix_allSubjects(parcellation_type,task) = p_temp(2,1);


        % investigate the driving inputs
        if ( task > 1 )

            % get all non-zero and non-diagonal absolut values
            mean_S1 = mean_Cmatrix_allSubjects(:,:,1);
            mean_S1 = mean_S1(mean_S1~=0);
            
            % get all non-zero and non-diagonal absolut values
            prec_S1 = 1./mean_vCmatrix_allSubjects(:,:,1);
            prec_S1 = prec_S1(prec_S1~=0);
            
            % get all ICC values
            ICC_Cmatrix_allSubjects_vec = ICC_Cmatrix_allSubjects(ICC_Cmatrix_allSubjects~=0);
            
            % compute the correlation between connection strength and ICC
            if ( mode == 1 )
                [R_temp,p_temp] = corrcoef(abs(mean_S1), ICC_Cmatrix_allSubjects_vec);
            elseif ( mode == 2 )
                [R_temp,p_temp] = corrcoef(prec_S1, ICC_Cmatrix_allSubjects_vec);
            end

            % asign the correlation coefficient
            R_Cmatrix_allSubjects(parcellation_type,task) = R_temp(2,1);
            p_Cmatrix_allSubjects(parcellation_type,task) = p_temp(2,1);

        end

        % print the correlation coefficient
        fprintf('\t %25s R(A) = %1.2f, p(A) = %1.3f | R(C) = %1.2f, p(C) = %1.3f \n',[task_title{task} ':'],round(R_Amatrix_allSubjects(parcellation_type,task),2),round(p_Amatrix_allSubjects(parcellation_type,task),3),round(R_Cmatrix_allSubjects(parcellation_type,task),2),round(p_Cmatrix_allSubjects(parcellation_type,task),3))
        
    end
end

end