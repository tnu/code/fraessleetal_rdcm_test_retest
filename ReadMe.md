# README

This README contains the minimal information on the analysis pipeline for the paper entitled "Test-retest reliability of regression dynamic causal modeling" published in Network Neuroscience. The repository is meant to provide all code utilized for data analysis.

# Contributors / Roles
|                               |                                             |
| ----------------------------- | ------------------------------------------- |
| Project lead / analysis:      | Stefan Frässle (PhD)                        |
| Supervising Prof.:            | Prof. Klaas Enno Stephan (MD Dr. med., PhD) |
| Abbreviation:                 | rDCM\_test\_retest                          |
| Date:                         | January 18, 2022                            |

# Summary
This repository contains all the code to perform the test-retest reliability analyses described in the publication "Test-retest reliability of regression dynamic causal modeling". Specifically, the test-retest HCP data are used and different methods of connectivity are applied. First, two versions of regression dynamic causal modeling (rDCM) are applied: (i) rDCM with fixed network architecture, and (ii) rDCM with sparsity constraints. Second, two functional connectivity algorithms are used: (i) Pearson's correlations, and (ii) regularized partial correlations. For these approaches, the study test three different metrics: (i) group-level consistency, (ii) test-retest reliability, and (iii) whole-pattern similarity. The code in this repository performs all of these analyses and then performes comparisons between the different connectivity methods. 

The project was conducted at the Translational Neuromodeling Unit (TNU) by the project lead (SF).

**Note:** The code in this repository will not run out-of-the-box because no data is provided along with the scripts. This is because data need to be obtained via HCP and we are not allowed to share the data ourselves. 

# Reference
Frässle S, Stephan KE. Test-retest reliability of regression dynamic causal modeling. *Network Neuroscience*, in press.

# Requirements
Software requirements of the project are MATLAB, Statistical Parametric Mapping (SPM12), several MATLAB toolboxes
