function collect_srDCM_parameter_estimates_MainExperiment(task,acq,model,parcellation_type)
% Collect the results for the different p0 values for each subject and 
% model from the whole-brain Dynamic Causal Models (DCMs) for the resting 
% state and task based datasets of the HCP dataset. Parameter estimates 
% have been computed using regression DCM (rDCM) with sparsity constraints.
% The function then summarizes the different reults and deletes the individual 
% p0 files for the sake of memory (if specified as an input argument).
% 
% Input:
%   subjects_analyze    -- subjects to include
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
% 	handedness         	-- left- or right-hand movement dataset
%   restrictInputs      -- fix inputs to the "correct" regions
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   delete_files        -- delete p0 values once summary file is created
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% session name
sesison_name = {'test','retest'};


% asign the foldernames
foldername = FilenameInfo.Data_Path;


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*'));...
    dir(fullfile(foldername,'3*')); dir(fullfile(foldername,'4*'));...
    dir(fullfile(foldername,'5*')); dir(fullfile(foldername,'6*'));...
    dir(fullfile(foldername,'7*')); dir(fullfile(foldername,'8*'));...
    dir(fullfile(foldername,'9*'))];


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};


% define missing DCMs cell
missing_DCMs = cell(1,length(sesison_name));


% consider all p0s
%p0_all = 1:9;
p0_all = 3:9;

    
% iterate over all subjects
for id = 1:length(Subject_List)

    % subject ID
    ID = Subject_List(id).name;
        
    % iterative over sessions
    for session = 1:length(sesison_name)

        % foldername
        foldername_rDCM = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},'firstlevel_dcm',parcellations{parcellation_type},'sparse_regressionDCM');
        
        % devine results array
        temp_output	= cell(1,length(p0_all));
        temp_F      = NaN(1,length(p0_all));
        counter     = 0;

        for p0_int = 1:length(p0_all)

            % file name
            p0_txt = ['00' num2str(p0_all(p0_int))];
            p0_txt = p0_txt(end-2:end);
            
            % search for the filename
            if ( isempty(acq) )
                filename_p0 = dir(fullfile(foldername_rDCM,[task_name{task} '_comb_*' p0_txt '.mat']));
                file_select = 1;
            else
                filename_p0	= [dir(fullfile(foldername_rDCM,[task_name{task} '1*' p0_txt '.mat'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*' p0_txt '.mat']))];
                file_select = acq;
            end
        
        
            % load the file
            if ( ~isempty(filename_p0) )
                
                % increase counter
                counter = counter + 1;

                % load the data
                temp = load(fullfile(foldername_rDCM,filename_p0(file_select).name));

                % asing output
                temp_output{p0_int}	= temp.output;
                temp_F(p0_int)      = temp.output.logF;
                
                % asign filename
                filename = filename_p0(file_select).name(1:end-8);

            else

                % foldername of normal rDCM
                foldername_rDCM_temp = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},'firstlevel_dcm',parcellations{parcellation_type},'regressionDCM');
                
                % file of normal rDCM
                filename_temp = dir(fullfile(foldername_rDCM_temp,[task_name{task} '_comb_*']));
                
                % store missing DCMs
                if ( ~isempty(filename_temp) )
                    missing_DCMs{session}(id,p0_int) = 1;
                end

            end
            
            % clear the p0 filename
            clear filename_p0 filename_temp

        end

        % clear the temp structure
        clear temp

        % get the best (max_F) solution and save the combined file
        if ( counter == length(p0_all) )

            % get the best
            [~,F_max_int] = max(temp_F);
            output        = temp_output{F_max_int};
            
            % save the results
            if ( ~isempty(output) )
                save(fullfile(foldername_rDCM, [filename '.mat']),'output')
            end
            
%             % delete the temporary files
%             if ( delete_files )
%                 for p0_int = p0_all
% 
%                     % get the filename
%                     p0_text = ['00' num2str(p0_int)];
%                     p0_text = p0_text(end-2:end);
% 
%                     % set the filename
%                     filename_p0 = dir(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM_' p0_text '.mat']));
% 
%                     % delete the p0 value
%                     delete(fullfile(foldername_rDCM, filename_p0(1).name))
% 
%                     % set the filename
%                     filename_options_p0 = dir(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM_' p0_text '_options.mat']));
% 
%                     % delete the p0 value
%                     delete(fullfile(foldername_rDCM, filename_options_p0(1).name))
% 
%                 end
%             end
        end

        % clear the output variable
        clear output options
        
    end
end


% display which DCMs are missing in which subjects
for session = 1:length(sesison_name)
    [id_miss,p0_miss] = find(missing_DCMs{session}==1);
    if ( ~isempty(id_miss) )
        for int = 1:length(id_miss)
            disp(['Session:' num2str(session) ' || Parcellation: ' num2str(parcellation_type) ' - Task: ' num2str(task) ' - Subject: ' num2str(id_miss(int)) ' - p0_ind: ' num2str(p0_all(p0_miss(int)))])
        end
    else
        disp('All DCMs estimated')
    end
end

end
