function extract_VOI_wb_MainExperiment(id,task,parcellation_type)
% Extracts BOLD signal time series from the gifti surface files of the
% Human Connectome Project (HCP) dataset for the test-retest analysis. This
% function takes the dense time series files (dtseries) and the
% parcellation (dlabel) file to generate a parcellated dtseries (ptseries) 
% file that contains the time series.
%
% Input:
%   id                  -- 
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path to the analysis code
addpath(FilenameInfo.Code_Path)

% add path to run command in MATLAB
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'osl-core-master')))


% go to the workbench folder
cd(fullfile(FilenameInfo.Wb_Path,'bin_rh_linux64'))


% set the parcellation
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};


% get the path to the HCP tnubank folder
hcp_folder  = FilenameInfo.LongtermStorage_Path;
data_folder = FilenameInfo.Data_Path;

% get the subject list
Subject_List = dir(fullfile(hcp_folder,'HCP_Retest','Raw','*'));
Subject_List = Subject_List(3:end);


% all different tasks
task_name = {'rfMRI_REST1_LR','rfMRI_REST1_RL','rfMRI_REST2_LR','rfMRI_REST2_RL',...
    'tfMRI_EMOTION_LR','tfMRI_EMOTION_RL','tfMRI_GAMBLING_LR','tfMRI_GAMBLING_RL',...
    'tfMRI_LANGUAGE_LR','tfMRI_LANGUAGE_RL','tfMRI_MOTOR_LR','tfMRI_MOTOR_RL',...
    'tfMRI_RELATIONAL_LR','tfMRI_RELATIONAL_RL','tfMRI_SOCIAL_LR','tfMRI_SOCIAL_RL',...
    'tfMRI_WM_LR','tfMRI_WM_RL'};


% check which subject to run
if ( isempty(id) )
    id_analyze = 1:length(Subject_List);
else
    id_analyze = id;
end


% check which tasks to run
if ( isempty(task) )
    task_analyze = 1:length(task);
else
    task_analyze = task;
end


% iterate over subjects
for id = id_analyze

    % subject ID
    ID = Subject_List(id).name;
    
    % output current subject id
    fprintf('Processing: %s \n',ID)
    
    % iterate over tasks
    for task = task_analyze

        % folder where dtseries file is stored
        nii_retest_folder = fullfile(hcp_folder,'HCP_Retest','Raw',ID,'MNINonLinear','Results',task_name{task});
        nii_test_folder   = fullfile(hcp_folder,'HCP','Raw',ID,'MNINonLinear','Results',task_name{task});

        % get the filename for the parcellation
        parcellation_file = dir(fullfile(FilenameInfo.Parcellation_Path,parcellations{parcellation_type},'*.dlabel.nii'));

        % folder where ptseries file will be stored
        ts_retest_folder = fullfile(data_folder,ID,'MNINonLinear','Results','retest',task_name{task},parcellations{parcellation_type});
        ts_test_folder   = fullfile(data_folder,ID,'MNINonLinear','Results','test',task_name{task},parcellations{parcellation_type});


        % make folder for test dataset
        if ( ~exist(ts_test_folder,'dir') )
            mkdir(ts_test_folder)
        end
        
        
        % filename
        if ( task < 5 )
            filename = [task_name{task} '_Atlas_hp2000_clean'];
        else
            filename = [task_name{task} '_Atlas'];
        end

        % input (dtseries), parcellation (dlabel), and output file (ptseries)
        infile  = fullfile(nii_test_folder,[filename '.dtseries.nii']);
        pfile   = fullfile(FilenameInfo.Parcellation_Path,parcellations{parcellation_type},parcellation_file(1).name);
        outfile = fullfile(ts_test_folder,[filename '.ptseries.nii']);

        % extract time series (test) from the parcellation
        try
            runcmd('./wb_command -cifti-parcellate %s %s COLUMN %s',infile,pfile,outfile)
        end


        % make folder for test dataset
        if ( ~exist(ts_retest_folder,'dir') )
            mkdir(ts_retest_folder)
        end

        % input (dtseries), parcellation (dlabel), and output file (ptseries)
        infile  = fullfile(nii_retest_folder,[filename '.dtseries.nii']);
        pfile   = fullfile(FilenameInfo.Parcellation_Path,parcellations{parcellation_type},parcellation_file(1).name);
        outfile = fullfile(ts_retest_folder,[filename '.ptseries.nii']);

        % extract time series (retest) from the parcellation
        try
            runcmd('./wb_command -cifti-parcellate %s %s COLUMN %s',infile,pfile,outfile)
        end

    end
end

end