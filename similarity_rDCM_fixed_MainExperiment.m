function similarity_rDCM_fixed_MainExperiment(variant,task,acq,parcellation_type,mode)
% Evaluates the mean effective connectivity parameters from the whole-brain 
% Dynamic Causal Models (DCMs) for the resting state dataset of the HCP 
% dataset. Importantly, it also computes the intra-class correlation
% coefficient.
%
% Input:
%   variant             -- (1) fixed, (2) sparsity
%   task                -- which task is analyzed
%   acq                 -- which acquisition is analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
%   mode                -- mode: (1) all connections, (2) top-1000 connections
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% folder for different rDCM variants
if ( variant == 1 )
	rdcm_folder = 'regressionDCM';
elseif ( variant == 2 )
    rdcm_folder = 'sparse_regressionDCM';
end


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},rdcm_folder);


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

% task names for title
task_title = {'REST','EMOTION','GAMBLING','LANGUAGE','MOTOR','RELATIONAL',...
    'SOCIAL','WM'};


% which tasks to analyze
if ( isempty(task) )
    task_analyze = 1:length(task_name);
else
    task_analyze = task;
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},rdcm_folder);

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,'Similarity_All_comb_Atlas.pdf'),'file') )
        delete(fullfile(figure_folder,'Similarity_All_comb_Atlas.pdf'))
    end
end


% iterate over tasks
for task = task_analyze

    % search for the filename
    if ( isempty(acq) )
        filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
        filename = filename(1).name;
    else
        filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
        filename = filename(acq).name;
    end


    % load the results array
    temp    = load(fullfile(foldername,filename));
    results = temp.results;


    % asign the data
    A_matrix_allSubjects     = results.A_Matrix_AllSubjects;
    C_matrix_allSubjects     = results.C_Matrix_AllSubjects;
    
    % initialize the temporary arrays
    mean_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));
    
    
    for int = 1:size(A_matrix_allSubjects,1)
        for int2 = 1:size(A_matrix_allSubjects,2)

            % subjects with data
            subject_vector = find(isfinite(A_matrix_allSubjects{int,int2,1}) & isfinite(A_matrix_allSubjects{int,int2,2}));

            % get the mean endogenous connection strength
            for int3 = 1:size(A_matrix_allSubjects,3)
                mean_Amatrix_allSubjects(int,int2,int3) = mean(A_matrix_allSubjects{int,int2,int3}(subject_vector));
            end
        end
    end
    
    % get the top-1000 connections
    if ( mode == 1 )
        matrix = ones(size(mean_Amatrix_allSubjects,1),size(mean_Amatrix_allSubjects,2));
        matrix = matrix - eye(size(matrix));
    else
        temp   = abs(mean_Amatrix_allSubjects(:,:,1) - diag(diag(mean_Amatrix_allSubjects(:,:,1))));
        matrix = compute_top_connections_MainExperiment(temp,[],1000);
    end
    
    
    % get endogenous parameters
    for int = 1:size(A_matrix_allSubjects,1)
        for int2 = 1:size(A_matrix_allSubjects,2)

            % subjects with data
            subject_vector = find(isfinite(A_matrix_allSubjects{int,int2,1}) & isfinite(A_matrix_allSubjects{int,int2,2}));
            
            % get the arrays for session 1 and 2
            A_matrix_allSubjects_S1(int,int2,:) = squeeze(A_matrix_allSubjects{int,int2,1}(subject_vector));
            A_matrix_allSubjects_S2(int,int2,:) = squeeze(A_matrix_allSubjects{int,int2,2}(subject_vector));

        end
    end
    
    % restrict to respective connections
    for int3 = 1:size(A_matrix_allSubjects_S1,3)
        A_matrix_allSubjects_S1(:,:,int3) = A_matrix_allSubjects_S1(:,:,int3) .* matrix;
        A_matrix_allSubjects_S2(:,:,int3) = A_matrix_allSubjects_S2(:,:,int3) .* matrix;
    end
    
    
    % get the driving inputs for task-fMRI
    if ( task > 1 )
        
        for int = 1:size(C_matrix_allSubjects,1)
            for int2 = 1:size(C_matrix_allSubjects,2)

                % subjects with data
                subject_vector = find(isfinite(C_matrix_allSubjects{int,int2,1}) & isfinite(C_matrix_allSubjects{int,int2,2}));
                
                % get the arrays for session 1 and 2
                C_matrix_allSubjects_S1(int,int2,:) = squeeze(C_matrix_allSubjects{int,int2,1}(subject_vector));
                C_matrix_allSubjects_S2(int,int2,:) = squeeze(C_matrix_allSubjects{int,int2,2}(subject_vector));

            end
        end
    end


    % empty array (correct)
    CorrectFalse_allSubjects = zeros(size(A_matrix_allSubjects_S1,3),2);
    Identified_allSubjects   = zeros(size(A_matrix_allSubjects_S1,3),2);
    accuracy                 = NaN(1,2);
    
    % empty array (permutation)
    CorrectFalse_allSubjects_Perm = zeros(size(A_matrix_allSubjects_S1,3),1000,2);
    Identified_allSubjects_Perm   = zeros(size(A_matrix_allSubjects_S1,3),1000,2);
    accuracy_Perm                 = NaN(1000,2);
    
    
    % ordering of sessions
    for ord = 1:2
        
        % similarity matrix
        Similarity_allSubjects      = NaN(size(A_matrix_allSubjects_S1,3),size(A_matrix_allSubjects_S2,3));
        Similarity_allSubjects_Perm = NaN(size(A_matrix_allSubjects_S1,3),size(A_matrix_allSubjects_S2,3),1000);
        
        % order of sessions
        switch ord
            case 1
                A_S1 = A_matrix_allSubjects_S1;
                A_S2 = A_matrix_allSubjects_S2;
            case 2
                A_S1 = A_matrix_allSubjects_S2;
                A_S2 = A_matrix_allSubjects_S1;
        end
        
        % compute similarity
        for subject_S1 = 1:size(A_S1,3)
            for subject_S2 = 1:size(A_S2,3)

                % get the arrays for both sessions
                temp_S1 = A_S1(:,:,subject_S1);
                temp_S2 = A_S2(:,:,subject_S2);

                % compute the correlation between the sessions
                sim_S1_S2 = corrcoef(temp_S1(matrix~=0),temp_S2(matrix~=0));

                % store the similarity values
                Similarity_allSubjects(subject_S1,subject_S2) = sim_S1_S2(1,2);

            end

            % get the subject that is closest
            [~,max_ind] = max(Similarity_allSubjects(subject_S1,:));
            Identified_allSubjects(subject_S1,ord) = max_ind;

            % check whether the same subject can be identified
            if ( max_ind == subject_S1)
                CorrectFalse_allSubjects(subject_S1,ord) = 1;
            end
        end
        
        % collate all results
        Identified_allSubjects_allTasks(subject_vector,task,ord) = Identified_allSubjects(:,ord);
    
        
        % define nice colormap
        [cbrewer_colormap] = cbrewer('seq', 'YlOrBr', 71, 'PCHIP');


        % compute accuracy
        accuracy(ord) = 100*sum(CorrectFalse_allSubjects(:,ord))/size(CorrectFalse_allSubjects,1);

        
        % plot average endogenous connectivity
        if ( ord == 1 )
            figure('units','normalized','outerposition',[0 0 1 1])
            imagesc(Similarity_allSubjects)
            hold on
            for id = 1:size(Similarity_allSubjects,1)
                rectangle('Position', [Identified_allSubjects(id)-0.5, id-0.5, 1, 1], 'EdgeColor', 'k', 'LineWidth', 1);
            end
            axis square
            colormap(cbrewer_colormap)
            set(gca,'xtick',[1 size(Similarity_allSubjects,2)])
            set(gca,'ytick',[1 size(Similarity_allSubjects,1)])
            ylabel('Subject ID (session 1)','FontSize',14)
            xlabel('Subject ID (session 2)','FontSize',14)
            title([task_title{task} ' | Similarity (ACC = ' num2str(round(accuracy(ord))) '%)'],'FontSize',18)
            print(gcf, '-dpdf', fullfile(figure_folder,['Similarity_All_comb_Atlas_temp' num2str(task)]), '-fillpage')
        end
        
        
        % set the seed for consistency
        rng(0)
        
        % compute empirical null distribution
        for NrPerm = 1:1000
            
            % create a random permutation
            rand_order = randperm(size(A_S2,3));
            
            for subject_S1 = 1:size(A_S1,3)
                for subject_S2 = rand_order

                    % get the arrays for both sessions
                    temp_S1 = A_S1(:,:,subject_S1);
                    temp_S2 = A_S2(:,:,subject_S2);

                    % compute the correlation between the sessions
                    sim_S1_S2 = corrcoef(temp_S1(matrix~=0),temp_S2(matrix~=0));

                    % store the similarity values
                    Similarity_allSubjects_Perm(subject_S1,subject_S2,NrPerm) = sim_S1_S2(1,2);

                end

                % get the subject that is closest
                [~,max_ind] = max(Similarity_allSubjects_Perm(subject_S1,:,NrPerm));
                Identified_allSubjects_Perm(subject_S1,NrPerm,ord) = max_ind;

                % check whether the same subject can be identified
                if ( max_ind == subject_S1)
                    CorrectFalse_allSubjects_Perm(subject_S1,NrPerm,ord) = 1;
                end
            end
        
            % collate all results
            Identified_allSubjects_allTasks_Perm(subject_vector,task,NrPerm,ord) = Identified_allSubjects_Perm(:,NrPerm,ord);
            
            % compute accuracy
            accuracy_Perm(NrPerm,ord) = 100*sum(CorrectFalse_allSubjects_Perm(:,NrPerm,ord))/size(CorrectFalse_allSubjects,1);
        
        end
    end
     
    % format specification of output
    formatSpec = '%17s | Accuracy (S1->S2): %4s%% (p = %4s, %2s/%2s) | Accuracy (S2->S1): %4s%% (p = %4s, %2s/%2s) \n';

    % print
    fprintf(formatSpec,task_title{task},num2str(round(accuracy(1),1)),...
        num2str(sum(accuracy_Perm(:,1)>accuracy(1))/size(accuracy_Perm,1)),...
        num2str(sum(CorrectFalse_allSubjects(:,1))),...
        num2str(size(CorrectFalse_allSubjects,1)),...
        num2str(round(accuracy(2),1)),...
        num2str(sum(accuracy_Perm(:,2)>accuracy(2))/size(accuracy_Perm,2)),...
        num2str(sum(CorrectFalse_allSubjects(:,2))),...
        num2str(size(CorrectFalse_allSubjects,1)))
    
    % clear all variables
    clear A_matrix_allSubjects_S1 A_matrix_allSubjects_S2 C_matrix_allSubjects_S1 C_matrix_allSubjects_S2
    
end

% line break
fprintf('\n')


% results array for majority vote
Identified_allSubjects_MV = NaN(size(Identified_allSubjects_allTasks,1),2,2);


% perform a majority vote
for ord = 1:2
    for subject = 1:size(Identified_allSubjects_allTasks,1)

        % remove zeros
        temp = Identified_allSubjects_allTasks(subject,:,ord);
        temp = temp(temp~=0);

        % find most common subject
        counter = zeros(1,size(Identified_allSubjects_allTasks,1));

        % count number of times subject is selected
        for int = 1:length(temp)
            for int2 = 1:size(Identified_allSubjects_allTasks,1)
                if ( temp(int) == int2 )
                    counter(int2) = counter(int2) + 1;
                end
            end
        end

        % find all maximal entries
        max_val = max(counter);
        subjects_all = find(counter == max_val);

        % store the selected subject
        if ( length(subjects_all) == 1 )
            Identified_allSubjects_MV(subject,1,ord) = subjects_all;
            Identified_allSubjects_MV(subject,2,ord) = subjects_all;
        else
            if ( sum(subjects_all == subject) ~= 0 )
                Identified_allSubjects_MV(subject,1,ord) = subject;
                Identified_allSubjects_MV(subject,2,ord) = subjects_all(find(subjects_all~=subject,1,'first'));
            else
                Identified_allSubjects_MV(subject,2,ord) = subjects_all(1);
                Identified_allSubjects_MV(subject,2,ord) = subjects_all(1);
            end
        end

        % check whether the same subject can be identified
        for int = 1:2
            if ( Identified_allSubjects_MV(subject,int,ord) == subject)
                CorrectFalse_allSubjects_MV(subject,int,ord) = 1;
            end
        end
    end
    
    % compute overall accuracy
    accuracy_MV(ord,:) = 100*sum(CorrectFalse_allSubjects_MV(:,:,ord))./size(CorrectFalse_allSubjects_MV,1);
    
end


% format specification of output
formatSpec = '%17s | Accuracy (S1->S2): %4s%% (%2s/%2s) | Accuracy (S2->S1): %4s%% (%2s/%2s) \n';

% diplay for how many subjects sessions were most similar
fprintf(formatSpec,'Majority Vote (o)',num2str(round(accuracy_MV(1,1),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,1,1))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)),...
        num2str(round(accuracy_MV(2,1),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,1,2))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)))
    
fprintf(formatSpec,'Majority Vote (p)',num2str(round(accuracy_MV(1,2),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,2,1))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)),...
        num2str(round(accuracy_MV(2,2),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,2,2))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)))


% append all PDFs
for task = task_analyze
    append_pdfs(fullfile(figure_folder,'Similarity_All_comb_Atlas.pdf'),...
        fullfile(figure_folder,['Similarity_All_comb_Atlas_temp' num2str(task) '.pdf']))
end
    
% delete the temporary files
delete(fullfile(figure_folder,'Similarity_All_comb_Atlas_temp*'))


end