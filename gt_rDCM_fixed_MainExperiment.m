function gt_rDCM_fixed_MainExperiment(task,acq,parcellation_type,model)
% Evaluates the summary statistics and reliability for the graph-theoretical
% measures from the whole-brain Dynamic Causal Models (DCMs) for the HCP 
% datasets. Importantly, it also computes the intra-class correlation
% coefficient.
%
% Input:
%   task                -- which task is analyzed
%   acq                 -- which acquisition is analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
%   model               -- model to be analyzed
%   computeGT           -- compute graph theoretical measues
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},'regressionDCM');


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};


% search for the filename
if ( isempty(acq) )
    filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
    filename = filename(1).name;
else
    filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
    filename = filename(acq).name;
end


% load the graph theory
temp    = load(fullfile(foldername,['GraphTheory_' filename]));
results = temp.results;


% initialize the temporary arrays
mean_is_allSubjects     = NaN(length(results(1,1).degree.is),2);
mean_os_allSubjects     = NaN(length(results(1,1).degree.is),2);
mean_str_allSubjects    = NaN(length(results(1,1).degree.is),2);
ICC_is_allSubjects     	= NaN(length(results(1,1).degree.is),1);
R_is_allSubjects        = NaN(length(results(1,1).degree.is),1);
ICC_os_allSubjects     	= NaN(length(results(1,1).degree.os),1);
R_os_allSubjects        = NaN(length(results(1,1).degree.os),1);
ICC_str_allSubjects     = NaN(length(results(1,1).degree.str),1);
R_str_allSubjects      	= NaN(length(results(1,1).degree.str),1);


% get graph theoretical measures
for int = 1:length(results(1).degree.is)
    
    % set the counter to zero
    nr_sub = 0;
    
    % get the GT value for all subjects
    for subject = 1:length(results)
        if ( isfield(results(subject).degree,'is') )
            nr_sub = nr_sub + 1;
            is_allSubjects(nr_sub,:)  = [results(subject,1).degree.is(int) results(subject,2).degree.is(int)];
            os_allSubjects(nr_sub,:)  = [results(subject,1).degree.os(int) results(subject,2).degree.os(int)];
            str_allSubjects(nr_sub,:) = [results(subject,1).degree.str(int) results(subject,2).degree.str(int)];
        end
    end
    
    % get the mean GT values
    mean_is_allSubjects(int,:)  = mean(is_allSubjects)';
    mean_os_allSubjects(int,:)  = mean(os_allSubjects)';
    mean_str_allSubjects(int,:) = mean(str_allSubjects)';
    
    % compute the intra-class correlation coefficient and correlation
    r	= ICC(is_allSubjects,'C-1');
    R   = corrcoef(is_allSubjects(:,1), is_allSubjects(:,2));
    
    % save ICC and R values
    ICC_is_allSubjects(int,1) = r;
    R_is_allSubjects(int,:)   = R(2,1);
    
    % compute the intra-class correlation coefficient and correlation
    r	= ICC(os_allSubjects,'C-1');
    R   = corrcoef(os_allSubjects(:,1), os_allSubjects(:,2));
    
    % save ICC and R values
    ICC_os_allSubjects(int,1) = r;
    R_os_allSubjects(int,:)   = R(2,1);
    
    % compute the intra-class correlation coefficient and correlation
    r	= ICC(str_allSubjects,'C-1');
    R   = corrcoef(str_allSubjects(:,1), str_allSubjects(:,2));
    
    % save ICC and R values
    ICC_str_allSubjects(int,1) = r;
    R_str_allSubjects(int,:)   = R(2,1);
    
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},'regressionDCM');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,['gt_' filename(1:end-4) '.pdf']),'file') )
        delete(fullfile(figure_folder,['gt_' filename(1:end-4) '.pdf']))
    end
end


% define nice colormpa
[cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
cbrewer_colormap   = flipud(cbrewer_colormap);
cbrewer_colormap   = cbrewer_colormap([1:33 36 39:71],:);

% iterate over different graph-theoretical measures
for measure = 1:3
    
    switch measure
        case 1
            mean_gt_measure = mean_is_allSubjects;
            icc_gt_measure  = ICC_is_allSubjects;
            gt_measure      = 'In strength';
        case 2
            mean_gt_measure = mean_os_allSubjects;
            icc_gt_measure  = ICC_os_allSubjects;
            gt_measure      = 'Out strength';
        case 3
            mean_gt_measure = mean_str_allSubjects;
            icc_gt_measure  = ICC_str_allSubjects;
            gt_measure      = 'In&Out strength';
    end


    % get the maximum value
    if ( task ~= 5 )
        c_limit = round(max(mean_gt_measure(:)),2);
    else
        c_limit = round(max(mean_gt_measure(:)),4);
    end


    % plot average endogenous connectivity
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(mean_gt_measure(:,1))
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis([-1*c_limit c_limit])
    set(gca,'xtick',1)
    set(gca,'ytick',[1 size(mean_gt_measure,1)/2 size(mean_gt_measure,1)])
    title([gt_measure ' (Session 1) [Hz]'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['gt_' filename(1:end-4) '_temp1']), '-fillpage')


    % plot average endogenous connectivity (significant)
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(mean_gt_measure(:,2))
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis([-1*c_limit c_limit])
    set(gca,'xtick',1)
    set(gca,'ytick',[1 size(mean_gt_measure,1)/2 size(mean_gt_measure,1)])
    title([gt_measure ' (Session 2) [Hz]'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['gt_' filename(1:end-4) '_temp2']), '-fillpage')


    % get the correlation of the group-average connectivity estimates
    [R_temp, p_temp] = corrcoef(mean_gt_measure(:,1),mean_gt_measure(:,2));

    % plot average endogenous connectivity (significant)
    figure('units','normalized','outerposition',[0 0 1 1])
    scatter(mean_gt_measure(:,1), mean_gt_measure(:,2), 50, [0.6 0.6 0.6], 'filled', 'MarkerEdgeColor', 'w')
    hold on
    plot([-1*c_limit c_limit],[-1*c_limit c_limit],'-','Color',cbrewer_colormap(65,:))
    if ( p_temp(2,1) < 0.001 )
        text(c_limit*0.8,-0.8*c_limit,['r = ' num2str(round(R_temp(2,1),3)) ', p < 0.001'],'FontSize',12)
    else
        text(c_limit*0.8,-0.8*c_limit,['r = ' num2str(round(R_temp(2,1),3)) ', p = ' num2str(round(p_temp(2,1),3))],'FontSize',12)
    end
    axis square
    box off
    xlim([-1*c_limit c_limit])
    ylim([-1*c_limit c_limit])
    xlabel('mean parameter estimate (Session 1) [Hz]')
    ylabel('mean parameter estimate (Session 2) [Hz]')
    title(['Group-level consistency (' gt_measure ')'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['gt_' filename(1:end-4) '_temp3']), '-fillpage')


    % plot the distribution of ein strengths
    figure('units','normalized','outerposition',[0 0 1 1]);
    histogram(icc_gt_measure,'BinWidth',0.05,'FaceColor',[0.5 0.5 0.5],'Normalization','pdf')
    pd = fitdist(icc_gt_measure,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
    hold on,
    area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
    ylimits = ylim();
    text(-0.8,0.8*ylimits(2),['mean ICC = ' num2str(round(mean(icc_gt_measure),3))],'FontSize',12)
    axis square
    box off
    set(gca,'xtick',[0 0.5 1])
    xlim([-1 1])
    xlabel('intra-class correlation coefficient (ICC)')
    ylabel('probability density function (PDF)')
    title(['Test-retest reliability (' gt_measure ')'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['gt_' filename(1:end-4) '_temp4']), '-fillpage')


    % append all PDFs
    append_pdfs(fullfile(figure_folder,['gt_' filename(1:end-4) '.pdf']),...
        fullfile(figure_folder,['gt_' filename(1:end-4) '_temp1.pdf']),...
        fullfile(figure_folder,['gt_' filename(1:end-4) '_temp2.pdf']),...
        fullfile(figure_folder,['gt_' filename(1:end-4) '_temp3.pdf']),...
        fullfile(figure_folder,['gt_' filename(1:end-4) '_temp4.pdf']))


    % delete the temporary files
    delete(fullfile(figure_folder,['gt_' filename(1:end-4) '_temp*']))
    
    % close all
    close all

end

end