function get_PC_estimates_MainExperiment(task,acq,parcellation_type)
% Get the individual parameter estimates from the whole-brain functional
% connectivity analysis for the resting state dataset of the HCP dataset.
% 
% This function reads the FC files that have been estimated. The function 
% stores the individual estimates.
% 
% Input:
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2021
% Copyright 2021 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% session name
sesison_name = {'test','retest'};


% asign the foldernames
foldername = FilenameInfo.Data_Path;


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*'));...
    dir(fullfile(foldername,'3*')); dir(fullfile(foldername,'4*'));...
    dir(fullfile(foldername,'5*')); dir(fullfile(foldername,'6*'));...
    dir(fullfile(foldername,'7*')); dir(fullfile(foldername,'8*'));...
    dir(fullfile(foldername,'9*'))];


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

    
% iterate over all subjects
for id = 1:length(Subject_List)

    % subject ID
    ID = Subject_List(id).name;
        
    % iterative over sessions
    for session = 1:length(sesison_name)

        % foldername
        foldername_fc = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},'firstlevel_fc',parcellations{parcellation_type},'partialCorr');
        
        % search for the filename
        if ( isempty(acq) )
            filename    = dir(fullfile(foldername_fc,[task_name{task} '_comb_*']));
            file_select = 1;
        else
            filename    = [dir(fullfile(foldername_fc,[task_name{task} '1*'])); dir(fullfile(foldername_fc,[task_name{task} '2*']))];
            file_select = acq;
        end
        
        % get the data
        if ( ~isempty(filename) )
            
            % select the filename
            filename = filename(file_select).name;
            
            try
            
                % load the DCM
                temp           = load(fullfile(foldername_fc,filename));
                output         = temp.output;
                
                % display subject name
                fprintf(['Subject: ' ID ' (' sesison_name{session} ' - ' num2str(id) ')\t - found \n'])
                
            catch
                
                % load a dummy result to get network size
                foldername_dummy = fullfile(foldername,'103818','MNINonLinear','Results',sesison_name{session},'firstlevel_fc',parcellations{parcellation_type},'partialCorr');
                file_dummy = dir(fullfile(foldername_dummy, filename));
                temp = load(fullfile(foldername_dummy,file_dummy(1).name));

                % define dummy functional connectivity
                output.partialCorrReg = NaN(size(temp.output.partialCorrReg,1),size(temp.output.partialCorrReg,2));
                
                % display subject name
                fprintf(['Subject: ' ID ' (' sesison_name{session} ' - ' num2str(id) ')\t - missing (wrong) \n'])
                
            end

            % clear the DCM file
            clear temp

        else

            % display subject name
            fprintf(['Subject: ' ID ' (' sesison_name{session} ' - ' num2str(id) ')\t - missing \n'])

            % load a dummy result to get network size
            foldername_dummy = fullfile(foldername,'103818','MNINonLinear','Results',sesison_name{session},'firstlevel_fc',parcellations{parcellation_type},'partialCorr');
            
            % search for the filename
            if ( isempty(acq) )
                filename = dir(fullfile(foldername_dummy,[task_name{task} '_comb_*']));
                filename = filename(1).name;
            else
                filename = [dir(fullfile(foldername_dummy,[task_name{task} '1*'])); dir(fullfile(foldername_fc,[task_name{task} '2*']))];
                filename = filename(acq).name;
            end
            
            file_dummy = dir(fullfile(foldername_dummy, filename));
            temp = load(fullfile(foldername_dummy,file_dummy(1).name));

            % define dummy functional connectivity
            output.partialCorrReg = NaN(size(temp.output.partialCorrReg,1),size(temp.output.partialCorrReg,2));

            % clear the DCM file
            clear temp

        end

        % define cells
        if ( id == 1 && session == 1 )
            partialCorrReg_AllSubjects    = cell(size(output.partialCorrReg,1),size(output.partialCorrReg,2),length(sesison_name));
        end

        % asign the a values for the endogenous parameters in each subject
        for int = 1:size(output.partialCorrReg,1)
            for int2 = 1:size(output.partialCorrReg,2)
                partialCorrReg_AllSubjects{int,int2,session} = [partialCorrReg_AllSubjects{int,int2,session}; output.partialCorrReg(int,int2)];
            end 
        end
        
        
        % asign the subject name
        if ( id == 1 && session == 1 )
            results.AllSubjects{id}  	= ID;
        else
            results.AllSubjects{end+1}  = ID;
        end
        
    end
end
    
% asign the results
results.partialCorrReg_AllSubjects = partialCorrReg_AllSubjects;


% create the results folder
if ( ~exist(fullfile(foldername,'grouplevel_fc',parcellations{parcellation_type},'partialCorr'),'dir') )
    mkdir(fullfile(foldername,'grouplevel_fc',parcellations{parcellation_type},'partialCorr'))
end

% save the estimated result
if ( ~isempty(results) )
    save(fullfile(foldername,'grouplevel_fc',parcellations{parcellation_type},'partialCorr', filename), 'results', '-v7.3')
end

end