function rX = round_for_plot_MainExperiment(X, N)
% Rounds a number in steps of 5 (exclusively for plotting to get nice axis
% labels).
% 
% For positive integers N, rounds to N digits to the right of the decimal 
% point. If N is zero, X is rounded to the nearest integer. If N is less 
% than zero, X is rounded to the left of the decimal point. N must be a 
% scalar integer.
% 
% Input:
%   X   -- value to round
%   N	-- decimal to round to
%   
% Output:
%   rX  -- rounded value
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2021
% Copyright 2021 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------

%   round_for_plot(X, N), for positive integers N, rounds to N digits to the right
%   of the decimal point. If N is zero, X is rounded to the nearest integer.
%   If N is less than zero, X is rounded to the left of the decimal point.
%   N must be a scalar integer.

% perform computation
rX = (floor((X * 10^(N+1))/5)*5)/10^(N+1);

end