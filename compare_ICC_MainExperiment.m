function compare_ICC_MainExperiment(variant,task,acq,parcellation_type)
% Compares the test-retest reliability of whole-brain connectivity patterns
% assessed using regression dynamic causal modeling and Pearson's
% correlations.
%
% Input:
%   variant             -- (1) fixed, (2) sparsity
%   task                -- tasks for which to compare ICCs
%   acq                 -- which sessions were used
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'circularGraph')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'micaopen-master','surfstat','surfstat_chicago')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'micaopen-master','surfstat','surfstat_addons')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% folder for different rDCM variants
if ( variant == 1 )
	rdcm_folder = 'regressionDCM';
    fc_folder   = 'pearsonCorr';
elseif ( variant == 2 )
    rdcm_folder = 'sparse_regressionDCM';
    fc_folder   = 'partialCorr';
end


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},rdcm_folder);


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

% task names for title
task_title = {'REST','EMOTION','GAMBLING','LANGUAGE','MOTOR','RELATIONAL',...
    'SOCIAL','WM'};


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},rdcm_folder,'rDCM_vs_FC');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,'TestRetest.pdf'),'file') )
        delete(fullfile(figure_folder,'TestRetest.pdf'))
    end
end


% which tasks to analyze
if ( isempty(task) )
    task_analyze = 1:length(task_name);
else
    task_analyze = task;
end

% create empty cells
ICC_Amatrix_allSubjects_allTask         = cell(task_analyze(end),1);
ICC_Amatrix_allSubjects_sig_allTask     = cell(task_analyze(end),1);
ICC_Amatrix_allSubjects_top_allTask     = cell(task_analyze(end),1);
ICC_Cmatrix_allSubjects_allTask         = cell(task_analyze(end),1);
ICC_Cmatrix_allSubjects_sig_allTask     = cell(task_analyze(end),1);
ICC_Cmatrix_allSubjects_top_allTask     = cell(task_analyze(end),1);
ICC_FCmatrix_allSubjects_allTask        = cell(task_analyze(end),1);
ICC_FCmatrix_allSubjects_sig_allTask    = cell(task_analyze(end),1);
ICC_FCmatrix_allSubjects_top_allTask	= cell(task_analyze(end),1);
Pp_Amatrix_allSubjects_allTask          = cell(task_analyze(end),1);
Pp_Cmatrix_allSubjects_allTask          = cell(task_analyze(end),1);


% iterate over tasks
for task = task_analyze
    
    % foldername for rDCM results
    foldernameICC_rDCM	= fullfile(FilenameInfo.Data_Path,'reliability',parcellations{parcellation_type},rdcm_folder);
    
    % search for the filename for rDCM
    if ( isempty(acq) )
        filename_rDCM = dir(fullfile(foldernameICC_rDCM,[task_name{task} '_comb_*']));
        filename_rDCM = filename_rDCM(1).name;
    else
        filename_rDCM = [dir(fullfile(foldernameICC_rDCM,[task_name{task} '1*'])); dir(fullfile(foldernameICC_rDCM,[task_name{task} '2*']))];
        filename_rDCM = filename_rDCM(acq).name;
    end
    
    % foldername for FC results
    foldernameICC_FC	= fullfile(FilenameInfo.Data_Path,'reliability',parcellations{parcellation_type},fc_folder);
    
    % search for the filename for FC
    if ( isempty(acq) )
        filename_FC = dir(fullfile(foldernameICC_FC,[task_name{task} '_comb_*']));
        filename_FC = filename_FC(1).name;
    else
        filename_FC = [dir(fullfile(foldernameICC_FC,[task_name{task} '1*'])); dir(fullfile(foldernameICC_FC,[task_name{task} '2*']))];
        filename_FC = filename_FC(acq).name;
    end
    

    % load the reliability results
    tempR_rDCM  = load(fullfile(foldernameICC_rDCM,filename_rDCM));
    tempR_FC	= load(fullfile(foldernameICC_FC,filename_FC));
    
    % load the summary results
    tempS_rDCM = load(fullfile(foldername,['Summary_' filename_rDCM]));
    tempS_FC   = load(fullfile(FilenameInfo.Data_Path,'grouplevel_fc',parcellations{parcellation_type},fc_folder,['Summary_' filename_FC]));
    
    
    % all ICC values
    ICC_Amatrix_allSubjects_allTasks_comp(:,:,task) = tempR_rDCM.results.ICC_Amatrix_allSubjects;
    ICC_FCmatrix_allSubjects_allTasks_comp(:,:,task) = tempR_FC.results.ICC_FCmatrix_allSubjects;
    
    % get ICCs (rDCM)
    temp = tempR_rDCM.results.ICC_Amatrix_allSubjects - diag(diag(tempR_rDCM.results.ICC_Amatrix_allSubjects));
    ICC_Amatrix_allSubjects_nodiag          = temp(temp~=0);
    ICC_Amatrix_allSubjects_allTask{task}   = ICC_Amatrix_allSubjects_nodiag;
    
    % get ICCs (FC)
    temp = tempR_FC.results.ICC_FCmatrix_allSubjects - diag(diag(tempR_FC.results.ICC_FCmatrix_allSubjects));
    ICC_FCmatrix_allSubjects_nodiag         = temp(temp~=0);
    ICC_FCmatrix_allSubjects_allTask{task}	= ICC_FCmatrix_allSubjects_nodiag;
    
    % all Pp values
    Pp_Amatrix_allSubjects_allTasks_comp(:,:,task) = tempR_rDCM.results.Pp_Amatrix_allSubjects;
    
    % get ICCs (rDCM)
    temp = tempR_rDCM.results.Pp_Amatrix_allSubjects - diag(diag(tempR_rDCM.results.Pp_Amatrix_allSubjects));
    Pp_Amatrix_allSubjects_nodiag           = temp(temp~=0);
    Pp_Amatrix_allSubjects_allTask{task}	= Pp_Amatrix_allSubjects_nodiag;
    
    
    % get ICCs for driving inputs (rDCM)
    if ( task > 1 )
        temp = tempR_rDCM.results.ICC_Cmatrix_allSubjects;
        ICC_Cmatrix_allSubjects_nodiag          = temp(temp~=0);
        ICC_Cmatrix_allSubjects_allTask{task}   = ICC_Cmatrix_allSubjects_nodiag;
        
        temp = tempR_rDCM.results.Pp_Cmatrix_allSubjects;
        Pp_Cmatrix_allSubjects_nodiag           = temp(temp~=0);
        Pp_Cmatrix_allSubjects_allTask{task}    = Pp_Cmatrix_allSubjects_nodiag;
    end
    
    
    % significance threshold (FDR-correction)
    if ( task == 1 )
        p_all               = tempS_rDCM.results.pVal_Amatrix_allSubjects(:);
        p_all               = p_all(isfinite(p_all));
        crit_p              = 0.05/length(p_all);
        threshold           = crit_p+eps;
    else
        p_all               = [tempS_rDCM.results.pVal_Amatrix_allSubjects(:); tempS_rDCM.results.pVal_Cmatrix_allSubjects(:)];
        p_all               = p_all(isfinite(p_all));
        crit_p              = 0.05/length(p_all);
        threshold           = crit_p+eps;
    end

    % choose all conections that are present
    pVal_Amatrix_allSubjects_select  = tempS_rDCM.results.pVal_Amatrix_allSubjects < threshold;
    pVal_FCmatrix_allSubjects_select = tempS_FC.results.pVal_FCmatrix_allSubjects < threshold;
    
    % choose all conections that are present
    if ( task > 1 )
        pVal_Cmatrix_allSubjects_select  = tempS_rDCM.results.pVal_Cmatrix_allSubjects < threshold;
    end
    
    
    % get all non-zero and non-diagonal absolut values for significant connections
    ICC_Amatrix_allSubjects_sig                 = tempR_rDCM.results.ICC_Amatrix_allSubjects .* pVal_Amatrix_allSubjects_select(:,:,1);
    temp                                        = ICC_Amatrix_allSubjects_sig - diag(diag(ICC_Amatrix_allSubjects_sig));
    ICC_Amatrix_allSubjects_nodiag_sig          = temp(temp~=0);
    ICC_Amatrix_allSubjects_sig_allTask{task}   = ICC_Amatrix_allSubjects_nodiag_sig;
    
    ICC_FCmatrix_allSubjects_sig                = tempR_FC.results.ICC_FCmatrix_allSubjects .* pVal_FCmatrix_allSubjects_select(:,:,1);
    temp                                        = ICC_FCmatrix_allSubjects_sig - diag(diag(ICC_FCmatrix_allSubjects_sig));
    ICC_FCmatrix_allSubjects_nodiag_sig         = temp(temp~=0);
    ICC_FCmatrix_allSubjects_sig_allTask{task}  = ICC_FCmatrix_allSubjects_nodiag_sig;
    
    % get all non-zero values for significant driving inputs
    if ( task > 1 )
        ICC_Cmatrix_allSubjects_sig                 = tempR_rDCM.results.ICC_Cmatrix_allSubjects .* pVal_Cmatrix_allSubjects_select(:,:,1);
        temp                                        = ICC_Cmatrix_allSubjects_sig;
        ICC_Cmatrix_allSubjects_nodiag_sig          = temp(temp~=0);
        ICC_Cmatrix_allSubjects_sig_allTask{task}   = ICC_Cmatrix_allSubjects_nodiag_sig;
    end
    
    
    % get all non-zero and non-diagonal absolut values for top connections
    temp                                        = abs(tempS_rDCM.results.mean_Amatrix_allSubjects(:,:,1) - diag(diag(tempS_rDCM.results.mean_Amatrix_allSubjects(:,:,1))));
    matrix_rDCM                                 = compute_top_connections_MainExperiment(temp,[],1000);
    ICC_Amatrix_allSubjects_top                 = tempR_rDCM.results.ICC_Amatrix_allSubjects .* (matrix_rDCM ~= 0);
    temp                                        = ICC_Amatrix_allSubjects_top - diag(diag(ICC_Amatrix_allSubjects_top));
    ICC_Amatrix_allSubjects_nodiag_top          = temp(temp~=0);
    ICC_Amatrix_allSubjects_top_allTask{task}	= ICC_Amatrix_allSubjects_nodiag_top;
    
    temp                                        = abs(tempS_rDCM.results.mean_Cmatrix_allSubjects(:,:,1));
    matrix_rDCM                                 = compute_top_connections_MainExperiment(temp,[],100);
    ICC_Cmatrix_allSubjects_top                 = tempR_rDCM.results.ICC_Cmatrix_allSubjects .* (matrix_rDCM ~= 0);
    temp                                        = ICC_Cmatrix_allSubjects_top;
    ICC_Cmatrix_allSubjects_nodiag_top          = temp(temp~=0);
    ICC_Cmatrix_allSubjects_top_allTask{task}	= ICC_Cmatrix_allSubjects_nodiag_top;
    
    temp                                        = abs(tempS_FC.results.mean_FCmatrix_allSubjects(:,:,1) - diag(diag(tempS_FC.results.mean_FCmatrix_allSubjects(:,:,1))));
    matrix_FC                                   = compute_top_connections_MainExperiment(temp,[],1000);
    ICC_FCmatrix_allSubjects_top                = tempR_FC.results.ICC_FCmatrix_allSubjects .* (matrix_FC ~= 0);
    temp                                        = ICC_FCmatrix_allSubjects_top - diag(diag(ICC_FCmatrix_allSubjects_top));
    ICC_FCmatrix_allSubjects_nodiag_top         = temp(temp~=0);
    ICC_FCmatrix_allSubjects_top_allTask{task}	= ICC_FCmatrix_allSubjects_nodiag_top;
    
    
    % plot the distribution of edge weights
    figure('units','normalized','outerposition',[0 0 1 1]);
    pd = fitdist(ICC_Amatrix_allSubjects_nodiag,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
    pd_FC = fitdist(ICC_FCmatrix_allSubjects_nodiag,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_FC = pdf(pd_FC,x);
    ha(2) = patch([x 1]',[y2_FC y2_FC(1)]',[0.8 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
    hold on
    ha(1) = patch([x 1]',[y2 y2(1)]',[0.4 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
    
    pd = fitdist(ICC_Amatrix_allSubjects_nodiag_sig,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
    pd_FC = fitdist(ICC_FCmatrix_allSubjects_nodiag_sig,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_FC = pdf(pd_FC,x);
    patch([x 1]',[y2_FC+2.5 y2_FC(1)+2.5]',[0.8 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
    patch([x 1]',[y2+2.5 y2(1)+2.5]',[0.4 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
    
    pd = fitdist(ICC_Amatrix_allSubjects_nodiag_top,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2 = pdf(pd,x);
    pd_FC = fitdist(ICC_FCmatrix_allSubjects_nodiag_top,'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_FC = pdf(pd_FC,x);
    patch([x 1]',[y2_FC+5 y2_FC(1)+5]',[0.8 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
    patch([x 1]',[y2+5 y2(1)+5]',[0.4 0.1 0.1],'FaceAlpha',0.4,'EdgeColor','none');
    
    axis square
    box off
    ylim([0 8])
    set(gca,'xtick',-1:0.5:1)
    set(gca,'ytick',[0 8])
    set(gca,'yticklabel',{'',''})
    legend(ha,{'rDCM','FC'},'Location','NW','FontSize',14)
    legend boxoff
    xlim([-1 1])
    xlabel('intra-class correlation coefficient (ICC)')
    ylabel('probability density function (PDF)')
    title([task_title{task} ' | Connectivity'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,'TestRetest_temp1'), '-fillpage')
    

    % append all PDFs
    append_pdfs(fullfile(figure_folder,'TestRetest.pdf'),...
        fullfile(figure_folder,'TestRetest_temp1.pdf'))


    % delete the temporary files
    delete(fullfile(figure_folder,'TestRetest_temp*'))
    
end


% write summary files for rDCM and FC
for mode = 1:2
    
    switch mode
        case 1
            folder_mode     = rdcm_folder;
            figure_mode     = 'All_comb_Atlas_rDCM';
            ICC_mode        = ICC_Amatrix_allSubjects_allTask;
            ICC_mode_sig	= ICC_Amatrix_allSubjects_sig_allTask;
            ICC_mode_top	= ICC_Amatrix_allSubjects_top_allTask;
            ICC_mode_all    = ICC_Amatrix_allSubjects_allTasks_comp;
            Pp_mode         = Pp_Amatrix_allSubjects_allTask;
            Pp_mode_all     = Pp_Amatrix_allSubjects_allTasks_comp;
        case 2
            folder_mode     = 'pearsonCorr';
            figure_mode     = 'All_comb_Atlas_FC';
            ICC_mode        = ICC_FCmatrix_allSubjects_allTask;
            ICC_mode_sig	= ICC_FCmatrix_allSubjects_sig_allTask;
            ICC_mode_top	= ICC_FCmatrix_allSubjects_top_allTask;
            ICC_mode_all    = ICC_FCmatrix_allSubjects_allTasks_comp;
    end

    % figure folder
    figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},folder_mode);

    % create the directory for the figures
    if ( ~exist(figure_folder,'dir') ) 
        mkdir(figure_folder)
    else
        if ( exist(fullfile(figure_folder,[figure_mode '.pdf']),'file') )
            delete(fullfile(figure_folder,[figure_mode '.pdf']))
        end
    end
    
    
    % define nice colormap
    [cbrewer_colormap] = cbrewer('div', 'RdYlBu', 71, 'PCHIP');
    cbrewer_colormap   = flipud(cbrewer_colormap);

    % plot the most reliable connections
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(tanh(mean(atanh(ICC_mode_all),3)))
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis([-1 1])
    set(gca,'xtick',[1 size(ICC_mode_all,2)/2 size(ICC_mode_all,2)])
    set(gca,'ytick',[1 size(ICC_mode_all,1)/2 size(ICC_mode_all,1)])
    xlabel('region (from)','FontSize',20)
    ylabel('region (to)','FontSize',20)
    title('Average (across-task) reliability','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp1.pdf']), '-fillpage')
    

    % define nice colormap
    [cbrewer_colormap] = cbrewer('seq', 'PuBu', 9, 'PCHIP');
    cbrewer_colormap   = flipud(cbrewer_colormap);

    % plot the most reliable connections
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(sum((ICC_mode_all > 0.75),3))
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis([0 8])
    set(gca,'xtick',[1 size(ICC_mode_all,2)/2 size(ICC_mode_all,2)])
    set(gca,'ytick',[1 size(ICC_mode_all,1)/2 size(ICC_mode_all,1)])
    xlabel('region (from)','FontSize',20)
    ylabel('region (to)','FontSize',20)
    title('# occurence excellent reliability','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp2.pdf']), '-fillpage')

    
    % compute how many connections have excellent reliability
    occ_rel = sum((ICC_mode_all > 0.75),3);
    
    % different conditions dependent on rDCM variant
    if ( variant == 1 )
        occ_rel_display = occ_rel==8;
    elseif ( variant == 2 )
        occ_rel_display = occ_rel>=6;
    end
    
    % get the average ICC for each node
    parc_data = mean(mean(ICC_mode_all,3),2);
    
    % get all nodes that have excellent reliability
    indices = unique([find(sum(occ_rel_display,2)~=0); find(sum(occ_rel_display)~=0)']);
    parc_data_occ = zeros(size(occ_rel_display,1),1);
    parc_data_occ(indices) = 1;
    
%     strrep(tempS_rDCM.results.AllRegions(indices),'_ROI','')'
    
    % get the Freesurfer surface
    S = SurfStatAvSurf({fullfile(FilenameInfo.Parcellation_Path, parcellations{parcellation_type}, 'Annotation', 'lh.pial'),...
        fullfile(FilenameInfo.Parcellation_Path, parcellations{parcellation_type}, 'Annotation', 'rh.pial')});
    
    % get annotation files
    lh_file = dir(fullfile(FilenameInfo.Parcellation_Path, parcellations{parcellation_type}, 'Annotation', 'lh.*.annot'));
    rh_file = dir(fullfile(FilenameInfo.Parcellation_Path, parcellations{parcellation_type}, 'Annotation', 'rh.*.annot'));
    
    % load annotation files
    [parc, ~, ~] = annot2classes(fullfile(FilenameInfo.Parcellation_Path, parcellations{parcellation_type}, 'Annotation', lh_file(1).name),...
        fullfile(FilenameInfo.Parcellation_Path, parcellations{parcellation_type}, 'Annotation', rh_file(1).name),1);
    
    % add the "NaN" entries
    parc_data       = [0; parc_data(1:length(parc_data)/2); 0; parc_data(length(parc_data)/2+1:end)];
    parc_data_occ   = [0; parc_data_occ(1:length(parc_data_occ)/2); 0; parc_data_occ(length(parc_data_occ)/2+1:end)];
    
    % convert parcellation data to vertex data
    vert_data     = BoSurfStatMakeParcelData(parc_data, S, parc);
    vert_data_occ = BoSurfStatMakeParcelData(parc_data_occ, S, parc);
    
    % create nice colormap
    [cbrewer_colormap] = cbrewer('seq', 'YlOrBr', 71, 'PCHIP');
    
    % plot the surface data
    figure('units','normalized','outerposition',[0 0 1 1]);
    BoSurfStat_calibrate4Views(vert_data, S, ...
             [0.12 0.55 0.4 0.4; 0.12 0.1 0.4 0.4; ...
             0.45 0.1 0.4 0.4; 0.45 0.55 0.4 0.4], ...
             1:4, [0 0.7], cbrewer_colormap)
    colorbar off
    orient(gcf, 'landscape')
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp3.pdf']), '-fillpage')
    
    
    % plot the surface data
    figure('units','normalized','outerposition',[0 0 1 1]);
    if ( ~isempty(indices) )
        BoSurfStat_calibrate4Views(vert_data_occ, S, ...
                 [0.12 0.55 0.4 0.4; 0.12 0.1 0.4 0.4; ...
                 0.45 0.1 0.4 0.4; 0.45 0.55 0.4 0.4], ...
                 1:4, [0 1], [1.0000 0.9686 0.9529; 0.5961 0.3176 0.4275])
        colorbar off
    end
    orient(gcf, 'landscape')
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp4.pdf']), '-fillpage')
    
    
    % create the connectogram
    occ_rel_display	= occ_rel_display - diag(diag(occ_rel_display));
    figure('units','normalized','outerposition',[0 0 1 1])
%     circularGraph(occ_rel_display,'Label',strrep(strrep(tempS_rDCM.results.AllRegions,'_ROI',''),'_','\_'));
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp5.pdf']), '-fillpage')
    
    
    % create a CIRCOS plot
%     create_CIRCOS_plot(FilenameInfo,parcellation_type,occ_rel_display,fullfile(figure_folder,figure_mode),tempS_rDCM.results.AllRegions)
    
    
    % print method
    disp([folder_mode ' (connectivity)'])
    
    % print the mean ICC
    for task = task_analyze
        
        fprintf('\t %25s ICC(A) = %1.2f [%1.2f %1.2f], ICC(Asig) = %1.2f [%1.2f %1.2f], ICC(Atop) = %1.2f [%1.2f %1.2f] \n',[task_title{task} ':'],round(tanh(mean(atanh(ICC_mode{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode{task}(:))) - 1.96 * std(atanh(ICC_mode{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode{task}(:))) + 1.96 * std(atanh(ICC_mode{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode_sig{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode_sig{task}(:))) - 1.96 * std(atanh(ICC_mode_sig{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode_sig{task}(:))) + 1.96 * std(atanh(ICC_mode_sig{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode_top{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode_top{task}(:))) - 1.96 * std(atanh(ICC_mode_top{task}(:)))),2),...
            round(tanh(mean(atanh(ICC_mode_top{task}(:))) + 1.96 * std(atanh(ICC_mode_top{task}(:)))),2))
    
    end
    
    % empty line
    fprintf('\n')
    
    % driving inputs
    if ( mode == 1 )
        
        % print method
        disp([folder_mode ' (inputs)'])

        % print the mean ICC
        for task = task_analyze
            if ( task > 1 )
                
                fprintf('\t %25s ICC(C) = %1.2f [%1.2f %1.2f], ICC(Csig) = %1.2f [%1.2f %1.2f], ICC(Ctop) = %1.2f [%1.2f %1.2f] \n',[task_title{task} ':'],round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_allTask{task}(:))) - 1.96 * std(atanh(ICC_Cmatrix_allSubjects_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_allTask{task}(:))) + 1.96 * std(atanh(ICC_Cmatrix_allSubjects_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_sig_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_sig_allTask{task}(:))) - 1.96 * std(atanh(ICC_Cmatrix_allSubjects_sig_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_sig_allTask{task}(:))) + 1.96 * std(atanh(ICC_Cmatrix_allSubjects_sig_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_top_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_top_allTask{task}(:))) - 1.96 * std(atanh(ICC_Cmatrix_allSubjects_top_allTask{task}(:)))),2),...
                    round(tanh(mean(atanh(ICC_Cmatrix_allSubjects_top_allTask{task}(:))) + 1.96 * std(atanh(ICC_Cmatrix_allSubjects_top_allTask{task}(:)))),2))
            end
        end
        
        % empty line
        fprintf('\n')
    end
    
    
    % define nice colormap
    [cbrewer_colormap] = cbrewer('div', 'RdBu', 89, 'PCHIP');
    cbrewer_colormap   = flipud(cbrewer_colormap);
    cbrewer_colormap   = cbrewer_colormap([1:38 54:89],:);

    % spacing
    delta_x = 4;

    % plot the distribution of edge weights
    figure('units','normalized','outerposition',[0 0 1 1]);
    for task = task_analyze
        if ( ~isempty(ICC_mode{task}) )
            pd_task = fitdist(ICC_mode{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
            ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
            hold on
        end
        h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
        set(h,'Rotation',270)
    end
    plot([-1 delta_x*task],[0 0],'k-')
    axis square
    box off
    set(gca,'ytick',-1:0.5:1)
    set(gca,'xtick',[])
    ylim([-1 1])
    xlim([-1 delta_x*task])
    ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
    xlabel('probability density function (PDF)','FontSize',14)
    title('Connectivity','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp6.pdf']), '-fillpage')


    % plot the distribution of edge weights
    figure('units','normalized','outerposition',[0 0 1 1]);
    for task = task_analyze
        if ( ~isempty(ICC_mode_sig{task}) )
            pd_task = fitdist(ICC_mode_sig{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
            ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
            hold on
            plot(delta_x*(task-1)-0.2+randn(length(ICC_mode_sig{task}),1)/20,ICC_mode_sig{task},'.','Color',[0.7 0.7 0.7])
        end
        h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
        set(h,'Rotation',270)
    end
    plot([-1 delta_x*task],[0 0],'k-')
    axis square
    box off
    set(gca,'ytick',-1:0.5:1)
    set(gca,'xtick',[])
    ylim([-1 1])
    xlim([-1 delta_x*task])
    ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
    xlabel('probability density function (PDF)','FontSize',14)
    title('Connectivity (sig)','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp7.pdf']), '-fillpage')


    % plot the distribution of edge weights
    figure('units','normalized','outerposition',[0 0 1 1]);
    for task = task_analyze
        if ( ~isempty(ICC_mode_top{task}) )
            pd_task = fitdist(ICC_mode_top{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
            ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
            hold on
            plot(delta_x*(task-1)-0.2+randn(length(ICC_mode_top{task}),1)/20,ICC_mode_top{task},'.','Color',[0.7 0.7 0.7])
        end
        h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
        set(h,'Rotation',270)
    end
    plot([-1 delta_x*task],[0 0],'k-')
    axis square
    box off
    set(gca,'ytick',-1:0.5:1)
    set(gca,'xtick',[])
    ylim([-1 1])
    xlim([-1 delta_x*task])
    ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
    xlabel('probability density function (PDF)','FontSize',14)
    title('Connectivity (top-1000)','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp8.pdf']), '-fillpage')
    
    
    % plot the distribution of Pp
    figure('units','normalized','outerposition',[0 0 1 1]);
    clear ha
    if ( mode == 1 )
        for task = task_analyze
            
            % define the bandwidth of the kernel
            if ( variant == 1 )
                if ( task == 1 )
                    bandwidth = 0.001;
                elseif ( task == 5 || task == 8 )
                    bandwidth = 0.00001;
                else
                    bandwidth = 0.0001;
                end
            else
                bandwidth = 0.001;
            end

            % plot the results
            if ( ~isempty(Pp_mode{task}) )
                subplot(3,3,task)
                pd_task = fitdist(Pp_mode{task},'Kernel','Bandwidth',bandwidth); x = 0:0.00001:0.03; y2_task = pdf(pd_task,x);
                ha(1) = patch([x x(1)]',[y2_task min([y2_task(1) y2_task(end)])]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
                hold on
            end

            % show the legend and labels
            legend(ha,task_title{task},'Location','NE')
            legend boxoff
            axis square
            box off
            if ( variant == 1 )
                if ( task ~= 5 && task ~= 8 )
                    set(gca,'xtick',[0 0.015])
                    set(gca,'xticklabel',[0 0.015],'FontSize',12)
                    xlim([0 0.015])
                else
                    set(gca,'xtick',[0 0.0005])
                    set(gca,'xticklabel',[0 0.0005],'FontSize',12)
                    xlim([0 0.0005])
                end
            else
                set(gca,'xtick',[0 0.03])
                set(gca,'xticklabel',[0 0.03],'FontSize',12)
                xlim([0 0.03])
            end
            
            % plot y- and x-label
            if ( task == 1 )
                xlabel('probability of difference','FontSize',16)
                ylabel('probability density function (PDF)','FontSize',16)
            end
        end
    end
    print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp9.pdf']), '-fillpage')

    % append all PDFs
    append_pdfs(fullfile(figure_folder,[figure_mode '.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp1.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp2.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp3.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp4.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp5.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp6.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp7.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp8.pdf']),...
        fullfile(figure_folder,[figure_mode '_temp9.pdf']))


    % delete the temporary files
    delete(fullfile(figure_folder,[figure_mode '_temp*']))
    
    
    % driving input parameters for rDCM on task-fMRI
    if ( mode == 1 && task > 1 )
        
        % data and def
        folder_mode     = 'regressionDCM';
        figure_mode     = 'All_comb_Atlas_rDCM';
        ICC_mode        = ICC_Cmatrix_allSubjects_allTask;
        ICC_mode_sig	= ICC_Cmatrix_allSubjects_sig_allTask;
        Pp_mode         = Pp_Cmatrix_allSubjects_allTask;
        
        % spacing
        delta_x = 4;
        
        % plot the distribution of edge weights
        figure('units','normalized','outerposition',[0 0 1 1]);
        for task = task_analyze
            if ( ~isempty(ICC_mode{task}) )
                pd_task = fitdist(ICC_mode{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
                ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
                hold on
                plot(delta_x*(task-1)-0.2+randn(length(ICC_mode{task}),1)/20,ICC_mode{task},'.','Color',[0.7 0.7 0.7])
            end
            h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
            set(h,'Rotation',270)
        end
        plot([-1 delta_x*task],[0 0],'k-')
        axis square
        box off
        set(gca,'ytick',-1:0.5:1)
        set(gca,'xtick',[])
        ylim([-1 1])
        xlim([-1 delta_x*task])
        ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
        xlabel('probability density function (PDF)','FontSize',14)
        title('Driving inputs','FontSize',18)
        print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp1.pdf']), '-fillpage')


        % plot the distribution of edge weights
        figure('units','normalized','outerposition',[0 0 1 1]);
        for task = task_analyze
            if ( ~isempty(ICC_mode_sig{task}) )
                pd_task = fitdist(ICC_mode_sig{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
                ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
                hold on
                plot(delta_x*(task-1)-0.2+randn(length(ICC_mode_sig{task}),1)/20,ICC_mode_sig{task},'.','Color',[0.7 0.7 0.7])
            end
            h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
            set(h,'Rotation',270)
        end
        plot([-1 delta_x*task],[0 0],'k-')
        axis square
        box off
        set(gca,'ytick',-1:0.5:1)
        set(gca,'xtick',[])
        ylim([-1 1])
        xlim([-1 delta_x*task])
        ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
        xlabel('probability density function (PDF)','FontSize',14)
        title('Driving inputs (sig)','FontSize',18)
        print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp2.pdf']), '-fillpage')
        
        
        % plot the probabilities of a difference
        figure('units','normalized','outerposition',[0 0 1 1]);
        clear ha
        for task = task_analyze
            
            % define the bandwidth of the kernel
            if ( variant == 1 )
                if ( task == 5 || task == 8 )
                    bandwidth = 0.001;
                else
                    bandwidth = 0.01;
                end
            else
                bandwidth = 0.01;
            end

            % plot the results
            if ( ~isempty(Pp_mode{task}) )
                subplot(3,3,task)
                pd_task = fitdist(Pp_mode{task},'Kernel','Bandwidth',bandwidth); x = 0:0.001:0.5; y2_task = pdf(pd_task,x);
                ha(1) = patch([x x(1)]',[y2_task min([y2_task(1) y2_task(end)])]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
                hold on
                legend(ha,task_title{task},'Location','NE')
                legend boxoff
            end

            % axis and labels
            axis square
            box off
            if ( variant == 1 )
                if ( task == 5 || task == 8 )
                    set(gca,'xtick',[0 0.05])
                    xlim([0 0.05])
                else
                    set(gca,'xtick',[0 0.5])
                    xlim([0 0.5])
                end
            else
                if ( task == 5 || task == 8 )
                    set(gca,'xtick',[0 0.2])
                    xlim([0 0.2])
                else
                    set(gca,'xtick',[0 0.5])
                    xlim([0 0.5])
                end
            end
                

            % plot y- and x-label
            if ( task == 2 )
                xlabel('probability of difference','FontSize',14)
                ylabel('probability density function (PDF)','FontSize',14)
            end
        end
        print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp3.pdf']), '-fillpage')
        
        
        % append all PDFs
        append_pdfs(fullfile(figure_folder,[figure_mode '.pdf']),...
            fullfile(figure_folder,[figure_mode '_temp1.pdf']),...
            fullfile(figure_folder,[figure_mode '_temp2.pdf']),...
            fullfile(figure_folder,[figure_mode '_temp3.pdf']))


        % delete the temporary files
        delete(fullfile(figure_folder,[figure_mode '_temp*']))
        
    end
end


% settings for combined plot
folder_mode     = [rdcm_folder '/rDCM_vs_FC'];
figure_mode     = 'All_comb_Atlas_rDCM_vs_FC';
ICC_mode        = ICC_Amatrix_allSubjects_allTask;
ICC_mode_sig	= ICC_Amatrix_allSubjects_sig_allTask;
ICC_mode_top	= ICC_Amatrix_allSubjects_top_allTask;
ICC_FC_mode     = ICC_FCmatrix_allSubjects_allTask;
ICC_FC_mode_sig	= ICC_FCmatrix_allSubjects_sig_allTask;
ICC_FC_mode_top	= ICC_FCmatrix_allSubjects_top_allTask;



% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},folder_mode);

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,[figure_mode '.pdf']),'file') )
        delete(fullfile(figure_folder,[figure_mode '.pdf']))
    end
end


% define nice colormap
[cbrewer_colormap] = cbrewer('div', 'RdBu', 89, 'PCHIP');
cbrewer_colormap   = flipud(cbrewer_colormap);
cbrewer_colormap   = cbrewer_colormap([1:38 54:89],:);

% spacing
delta_x = 4;

% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
for task = task_analyze
    if ( ~isempty(ICC_mode{task}) )
        pd_task    = fitdist(ICC_mode{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
        pd_task_FC = fitdist(ICC_FC_mode{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task_FC = pdf(pd_task_FC,x);
        ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
        hold on
        ha(task) = patch([y2_task_FC+delta_x*(task-1) y2_task_FC(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:)*0.5,'FaceAlpha',0.4,'EdgeColor','none');
        plot(y2_task(51:end)+delta_x*(task-1),x(51:end),'-.','Color',[0.1 0.1 0.1]);
        plot(y2_task_FC(51:end)+delta_x*(task-1),x(51:end),'--','Color',[0.1 0.1 0.1]);
    end
    h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
    set(h,'Rotation',270)
end
plot([-1 delta_x*task],[0 0],'k-')
axis square
box off
set(gca,'ytick',-1:0.5:1)
set(gca,'xtick',[])
ylim([-1 1])
xlim([-1 delta_x*task])
ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
xlabel('probability density function (PDF)','FontSize',14)
title('Connectivity','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp1.pdf']), '-fillpage')


% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
for task = task_analyze
    if ( ~isempty(ICC_mode_sig{task}) )
        pd_task    = fitdist(ICC_mode_sig{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
        pd_task_FC = fitdist(ICC_FC_mode_sig{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task_FC = pdf(pd_task_FC,x);
        ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
        hold on
        ha(task) = patch([y2_task_FC+delta_x*(task-1) y2_task_FC(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:)*0.5,'FaceAlpha',0.4,'EdgeColor','none');
        plot(y2_task(51:end)+delta_x*(task-1),x(51:end),'-.','Color',[0.1 0.1 0.1]);
        plot(y2_task_FC(51:end)+delta_x*(task-1),x(51:end),'--','Color',[0.1 0.1 0.1]);
    end
    h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
    set(h,'Rotation',270)
end
plot([-1 delta_x*task],[0 0],'k-')
axis square
box off
set(gca,'ytick',-1:0.5:1)
set(gca,'xtick',[])
ylim([-1 1])
xlim([-1 delta_x*task])
ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
xlabel('probability density function (PDF)','FontSize',14)
title('Connectivity (sig)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp2.pdf']), '-fillpage')


% plot the distribution of edge weights
figure('units','normalized','outerposition',[0 0 1 1]);
for task = task_analyze
    if ( ~isempty(ICC_mode_top{task}) )
        pd_task = fitdist(ICC_mode_top{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task = pdf(pd_task,x);
        pd_task_FC = fitdist(ICC_FC_mode_top{task},'Kernel','Bandwidth',0.1); x = -1:0.01:1; y2_task_FC = pdf(pd_task_FC,x);
        ha(task) = patch([y2_task+delta_x*(task-1) y2_task(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:),'FaceAlpha',0.4,'EdgeColor','none');
        hold on
        ha(task) = patch([y2_task_FC+delta_x*(task-1) y2_task_FC(1)+delta_x*(task-1)]',[x 1]',cbrewer_colormap(1+10*(task-1),:)*0.5,'FaceAlpha',0.4,'EdgeColor','none');
        plot(y2_task(51:end)+delta_x*(task-1),x(51:end),'-.','Color',[0.1 0.1 0.1]);
        plot(y2_task_FC(51:end)+delta_x*(task-1),x(51:end),'--','Color',[0.1 0.1 0.1]);
    end
    h = text(delta_x*(task-1),-0.55,task_title{task},'FontSize',14);
    set(h,'Rotation',270)
end
plot([-1 delta_x*task],[0 0],'k-')
axis square
box off
set(gca,'ytick',-1:0.5:1)
set(gca,'xtick',[])
ylim([-1 1])
xlim([-1 delta_x*task])
ylabel('intra-class correlation coefficient (ICC)','FontSize',14)
xlabel('probability density function (PDF)','FontSize',14)
title('Connectivity (top-1000)','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,[figure_mode '_temp3.pdf']), '-fillpage')

% append all PDFs
append_pdfs(fullfile(figure_folder,[figure_mode '.pdf']),...
    fullfile(figure_folder,[figure_mode '_temp1.pdf']),...
    fullfile(figure_folder,[figure_mode '_temp2.pdf']),...
    fullfile(figure_folder,[figure_mode '_temp3.pdf']))


% delete the temporary files
delete(fullfile(figure_folder,[figure_mode '_temp*']))
