function estimate_srDCM_MainExperiment(id,task,acq_combine,model,session,parcellation_type,p0_ind)
% Infer whole-brain effective connectivity for the resting-state dataset of
% the HCP dataset using regression DCM with sparsity constraints.
% 
% This function prepares the respective DCM model. Futher, it applies rDCM 
% to the model, which means assuming a fully (all-to-all) connected model,
% which is then pruned during model inversion to yield a sparse
% representation of network connectivity.
% 
% Input:
%   id                 	-- subject to analyze
%   task                -- task to analyze
%   acq_combine         -- combine different acquisitions per task
%   model               -- model to analyze
%   session             -- session: (1) test, (2) retest
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   p0_ind              -- prior on sparsity constraint
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add the rDCM toolbox
addpath(genpath(fullfile(FilenameInfo.TAPAS_Path,'rDCM')))
addpath(FilenameInfo.SPM_Path)
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% initialize spm
spm_jobman('initcfg')


% set the parcellation
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% session name
sesison_name     = {'test','retest'};
hcp_session_name = {'HCP','HCP_Retest'};


% asign the foldernames
foldername = FilenameInfo.Data_Path;
hcp_folder = FilenameInfo.LongtermStorage_Path;


% get the subject list
Subject_List = dir(fullfile(foldername,'*'));
Subject_List = Subject_List(3:end);


% choose which subjects to analyze
if ( isempty(id) )
    id_analyze = 1:length(Subject_List);
else
    id_analyze = id;
end

% choose which session to analyze
if ( isempty(id) )
    session_analyze = 1:length(sesison_name);
else
    session_analyze = session;
end

% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

% condition filenames for difference tasks
condition_file    = cell(1,length(task_name));
condition_file{1} = {''};
condition_file{2} = {'fear.txt','neut.txt'};
condition_file{3} = {'win_event.txt','loss_event.txt','neut_event.txt'};
condition_file{4} = {'story.txt','math.txt'};
condition_file{5} = {'cue.txt','lf.txt','rf.txt','lh.txt','rh.txt','t.txt'};
condition_file{6} = {'relation.txt','match.txt'};
condition_file{7} = {'mental_resp.txt','other_resp.txt'};
condition_file{8} = {'0bk_body.txt','0bk_faces.txt','0bk_places.txt','0bk_tools.txt','2bk_body.txt','2bk_faces.txt','2bk_places.txt','2bk_tools.txt'};

% condition names for difference tasks
condition_name    = cell(1,length(task_name));
condition_name{1} = {''};
condition_name{2} = {'faces','shapes'};
condition_name{3} = {'win','loss','neutral'};
condition_name{4} = {'story','math'};
condition_name{5} = {'cue','left_foot','right_foot','left_hand','right_hand','tongue'};
condition_name{6} = {'relation','match'};
condition_name{7} = {'mental','other'};
condition_name{8} = {'0bk_body','0bk_faces','0bk_places','0bk_tools','2bk_body','2bk_faces','2bk_places','2bk_tools'};


% iterate overs subjects
for id = id_analyze
    
    % subject ID
    ID = Subject_List(id).name;
    
    % iterate over sessions
    for session = session_analyze
        
        % print progress
        fprintf(['\nProcessing: ' ID ' (' sesison_name{session} ')\n'])
        
        
        % clear relevant files
        clear DCM 
        clear output 
        clear options


        % define the standard option settings
        options.scale      = 1;
        options.estimateVL = 0;
        
        
        % display progress
        disp('Loading data...')
        
        
        % all folders
        aquisition_folder_all = dir(fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},[task_name{task} '*']));
        
        
        % which acquisitions to combine
        if ( isempty(acq_combine) )
            acq_combine_all = 1:length(aquisition_folder_all);
        else
            acq_combine_all = acq_combine;
        end
        
        
        % combine LR and RL acquisition or use only one
        for acquisition = acq_combine_all
    
            % foldername
            foldername_id = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},aquisition_folder_all(acquisition).name,parcellations{parcellation_type});
            
            % filename
            if ( task < 2 )
                filename = [aquisition_folder_all(acquisition).name '_Atlas_hp2000_clean'];
            else
                filename = [aquisition_folder_all(acquisition).name '_Atlas'];
            end
            
            
            % get the time series
            temp = ft_read_cifti(fullfile(foldername_id,[filename '.ptseries.nii']),'mapname','array');

            % specify the TR
            DCM.Y.dt  = temp.time(2)-temp.time(1);

            % specify the Y component of the DCM file
            DCM.Y.X0 = [];
            
            % specify empty cell array
            name = cell(1,size(temp.ptseries,1));
            
            % asign the data to the Y structure
            for i = 1:size(temp.ptseries,1)
                Y(:,i)  = temp.ptseries(i,:)';
                name{i} = temp.label{i};
            end
            
            % number of scans per acquisiton
            nr_scans = size(Y,1);
            
            % asign the data to the DCM.Y structure
            if ( acquisition == 1 )
                DCM.Y.y    = Y;
                DCM.Y.name = name;
            else
                DCM.Y.y    = [DCM.Y.y; Y];
            end
        end
            
            
        % diplay progress
        disp(['Found number of regions: ' num2str(size(DCM.Y.y,2))])
        disp(['Found number of scans (per region): ' num2str(size(DCM.Y.y,1))])
        
        
        % number of regions
        DCM.n = size(DCM.Y.y,2);

        % number of time points
        DCM.v = size(DCM.Y.y,1);


        % specify empty driving input
        if ( task == 1 )
            DCM.U.u     = zeros(size(DCM.Y.y,1)*16, 1);
            DCM.U.name  = {'null'};
            DCM.U.dt    = DCM.Y.dt/16;
        else
            
            % define driving inputs
            DCM.U.u = zeros(size(DCM.Y.y,1)*16,length(condition_file{task}));
            
            % start times
            start_time = [1 nr_scans*16+1];
            
            % combine or don't combine acquisitions
            for acquisition = acq_combine_all
                
                % foldername
                foldername_ev = fullfile(hcp_folder,hcp_session_name{2},'Raw',ID,'MNINonLinear','Results',aquisition_folder_all(acquisition).name,'EVs');
                
                % create regressor for each condition
                for cond = 1:length(condition_file{task})
                    
                    % load onsets and durations of conditions
                    exp_details = textread(fullfile(foldername_ev,condition_file{task}{cond}));
                    
                    % iterate over blocks
                    for block = 1:size(exp_details,1)
                        
                        % block start and block end
                        block_start = start_time(acquisition)+round((exp_details(block,1)/DCM.Y.dt)*16);
                        block_end   = block_start+round((exp_details(block,2)*16/DCM.Y.dt));
                        
                        % check whether the length of the block is too long
                        if ( block_end > acquisition*nr_scans*16 )
                            block_end = acquisition*nr_scans*16;
                        end
                        
                        % define block in input
                        DCM.U.u(block_start:block_end,cond) = 1;
                    
                    end
                end
            end
            
            % specify the name
            DCM.U.name = condition_name{task};
            
            % specify the sampling rate
            DCM.U.dt = DCM.Y.dt/16;
            
        end


        % DCM parameters
        DCM.delays = repmat(DCM.Y.dt/2,DCM.n,1);
        
        % asign echo time
        DCM.TE = 0.033;
        
        
        % DCM options
        DCM.options.nonlinear  = 0;
        DCM.options.two_state  = 0;
        DCM.options.stochastic = 0;
        DCM.options.nograph    = 0;
        
        
        % connectivity structure of respective model:
        %   Model 1: fully connected
        if ( task == 1 )
            if ( model == 1)
                DCM.a = ones(DCM.n);
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = zeros(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
            end
        else
            if ( model == 1)
                DCM.a = ones(DCM.n);
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = ones(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
            end
        end

        
        % dcm folder
        dcm_folder = fullfile(foldername,ID,'MNINonLinear','Results',sesison_name{session},'firstlevel_dcm',parcellations{parcellation_type});
        
        % check whether the folder exists
        if ( ~exist(dcm_folder,'dir') )
            mkdir(dcm_folder)
        end


        % detrend and scale the data
        if ( options.scale )

            % detrend data
            DCM.Y.y = spm_detrend(DCM.Y.y);

            % scale data
            scale_factor   = max(max((DCM.Y.y))) - min(min((DCM.Y.y)));
            scale_factor   = 4/max(scale_factor,4);
            DCM.Y.y        = DCM.Y.y*scale_factor;

        end


        % set the prior type of the endogenous matrix
        DCM.options.wide_priors = 0;


        % create the directory
        if ( ~exist(fullfile(dcm_folder, 'sparse_regressionDCM'),'dir') )
            mkdir(fullfile(dcm_folder, 'sparse_regressionDCM'))
        end


        % display the progress
        disp(['Evaluating subject ' ID ' - model ' num2str(model)])
        disp(' ')


        % shift the input regressor slightly
        options.u_shift    = 0;
        options.filter_str = 4;
        
        % do not compute the signal
        options.compute_signal = 0;
        
        
        % specify all the p0 values
        p0_array = 0.1:0.1:0.9;
        
        
        % specify the options for the rDCM analysis
        options.p0_all          = p0_array(p0_ind);
        options.iter            = 2;
        options.p0_inform       = 0;
        
        
        % prune inputs, but keep no inputs for rs-fMRI data
        if ( task == 1 )
            options.restrictInputs  = 1;
        else
            options.restrictInputs  = 0;
        end
        
        
        % store in intermediate file, if only single p0 is estimated
        if ( length(p0_ind) == 1 )
            p0_txt = ['00' num2str(p0_ind)];
            p0_txt = ['_' p0_txt(end-2:end)];
        else
            p0_txt = '';
        end

        
        % empirical analysis
        type = 'r';


        % get time
        currentTimer = tic;
        
        
        % run the rDCM analysis
        [output, options] = tapas_rdcm_estimate(DCM, type, options, 2);

        % output elapsed time
        time_rDCM = toc(currentTimer);
        disp(['Elapsed time is ' num2str(time_rDCM) ' seconds.'])


        % store the estimation time
        output.time_rDCM = time_rDCM;
        
        
        % remove unnecessary fields
        output = rmfield(output,'allParam');
        output = rmfield(output,'temp');
        output = rmfield(output,'statistics');
        
        
        % get the number of regions and inputs
        nr = size(output.Ep.A,1);
        nu = size(output.Ep.C,2);
        
        % obtain the posterior variances for each region
        for k = 1:nr
            
            % obtain diagonal from the region-wise covariance matrix
            Cpdiag = diag(output.sN{k});
            
            % get variances of endogenous parameters
            output.Vp.A(k,:) = Cpdiag(1:nr);
            
            % get variances of driving input parameters
            if ( task > 1 )
                
                % get the driving input variances
                output.Vp.C(k,:) = Cpdiag(nr+1:nr+nu);
                
            end
            
        end
        
          
        % remove the variances
        output = rmfield(output,'sN');
        
        
        % check if only one session or multiple sessions
        if ( length(acq_combine_all) ~= 1)
            if ( task < 2 )
                filename = [task_name{task} '_comb_Atlas_hp2000_clean'];
            else
                filename = [task_name{task} '_comb_Atlas'];
            end
        end
        
        % save the estimated results
        if ( ~isempty(output) )
            save(fullfile(dcm_folder, 'sparse_regressionDCM', [filename '_rDCM' p0_txt '.mat']),'output','-v7.3')
        end
    
    end
end

end
