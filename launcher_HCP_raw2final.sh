#!/bin/bash

# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------
# 
# This bash script comprises the full analysis pipeline associated with the following paper / project:
#
# Title: HCP_TestRetest
# Authors: Stefan Frässle, Klaas E. Stephan
#
# In brief, the analysis comprises three different steps:
#    (i)   group-level consistency of rDCM
#    (ii)  test-retest reliability of rDCM
#    (iii) comparison with functional connectivity measures
#
# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------


# -----------------------------------------
# Environment
# -----------------------------------------


# switch to new software stack (mainly to enable ghostscript)
. /cluster/apps/local/env2lmod.sh


# Load the ghostscript and matlab module
module load ghostscript/9.21
module load matlab


# output the new software stack (should have ghostscript and Matlab included)
module list


# create logs folder
mkdir logs



# -----------------------------------------
# rDCM analysis
# -----------------------------------------


# extract the BOLD signal time series
for parcellation in 1 4
do
	for task in {1..18}
	do
		bsub -W 4:00 -J "job_BOLD_$task" matlab -singleCompThread -r "extract_VOI_wb_MainExperiment([],$task,$parcellation)"
	done
done


# estimate rDCMs for both parcellations, all subjects and tasks, and both sessions
for parcellation in 1 4
do
	for ID in {1..45}
	do
		for task in {1..8}
		do
			for session in {1..2}
			do
				bsub -W 4:00 -J "job_rDCM_$parcellation,$ID,$task,$session" -w 'ended("job_BOLD_*")' -R "rusage[mem=20000]" matlab -singleCompThread -r "estimate_rDCM_fixed_MainExperiment($ID,$task,[],1,$session,$parcellation)"
			done
		done
	done
done


# collect all the rDCM results
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 4:00 -J "job_collect_rDCM_$task,$parcellation" -oo "logs/log_collect_$task,$parcellation" -w 'ended("job_rDCM_*")' matlab -singleCompThread -r "get_rDCM_parameter_estimates_fixed_MainExperiment($task,[],1,$parcellation)"
	done
done


# summarize all the rDCM results
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 4:00 -J "job_summary_rDCM_$task,$parcellation" -oo "logs/log_summary_$task,$parcellation" -w 'ended("job_collect_rDCM_*")' matlab -singleCompThread -r "summarize_rDCM_fixed_MainExperiment($task,[],$parcellation,1,0)"
	done
done


# compute the within-subject similarity
for parcellation in 1 4
do
	bsub -W 4:00 -J "job_similarity_rDCM_$parcellation" -oo "logs/log_similarity_$parcellation" -w 'ended("job_summary_rDCM_*")' matlab -singleCompThread -r "similarity_rDCM_fixed_MainExperiment(1,[],[],$parcellation,1)"
done



# -----------------------------------------
# Sparse rDCM analysis
# -----------------------------------------


# estimate rDCMs with sparsity constraints for both parcellations, all p0s, subjects and tasks, and both sessions
for parcellation in 1 4
do
	for ID in {1..45}
	do
		for task in {1..8}
		do
			for session in {1..2}
			do
				for p0 in {1..9}
				do
					bsub -W 12:00 -J "job_srDCM_$parcellation,$ID,$task,$session,$p0" -w 'ended("job_similarity_rDCM_*")' -R "rusage[mem=20000]" matlab -singleCompThread -r "estimate_srDCM_MainExperiment($ID,$task,[],1,$session,$parcellation,$p0)"
				done
			done
		done
	done
done


# collect the results for the different p0 values
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 12:00 -J "job_p0_srDCM_$parcellation,$task" -w 'ended("job_srDCM_*")' -R "rusage[mem=20000]" matlab -singleCompThread -r "collect_srDCM_parameter_estimates_MainExperiment($task,[],1,$parcellation)"
	done
done


# collect all the srDCM results
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 4:00 -J "job_collect_srDCM_$task,$parcellation" -oo "logs/log_collect_srDCM_$task,$parcellation" -w 'ended("job_p0_*")' matlab -singleCompThread -r "get_srDCM_parameter_estimates_MainExperiment($task,[],1,$parcellation)"
	done
done


# summarize all the srDCM results
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 4:00 -J "job_summary_srDCM_$task,$parcellation" -oo "logs/log_summary_srDCM_$task,$parcellation" -w 'ended("job_collect_srDCM_*")' matlab -singleCompThread -r "summarize_srDCM_MainExperiment($task,[],$parcellation,1,0)"
	done
done


# compute the within-subject similarity of srDCM
for parcellation in 1 4
do
	bsub -W 4:00 -J "job_similarity_srDCM_$parcellation" -oo "logs/log_similarity_srDCM_$parcellation" -w 'ended("job_summary_srDCM_*")' matlab -singleCompThread -r "similarity_rDCM_fixed_MainExperiment(2,[],[],$parcellation,1)"
done



# -----------------------------------------
# FC analysis
# -----------------------------------------


# estimate functional connectivity for both parcellations, all subjects and tasks, and both sessions
for parcellation in 1 4
do
	for ID in {1..45}
	do
		for task in {1..8}
		do
			for session in {1..2}
			do
				bsub -W 1:00 -J "job_FC_$parcellation,$ID,$task,$session" -w 'ended("job_similarity_srDCM_*")' matlab -singleCompThread -r "estimate_FC_MainExperiment($ID,$task,[],$session,$parcellation)"
				bsub -W 1:00 -J "job_PC_$parcellation,$ID,$task,$session" -w 'ended("job_similarity_srDCM_*")' matlab -singleCompThread -r "estimate_PC_MainExperiment($ID,$task,[],$session,$parcellation)"
			done
		done
	done
done


# collect all the FC results
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 4:00 -J "job_collect_FC_$task,$parcellation" -oo "logs/log_collect_FC_$task,$parcellation" -w 'ended("job_FC_*")' matlab -singleCompThread -r "get_FC_estimates_MainExperiment($task,[],$parcellation)"
		bsub -W 4:00 -J "job_collect_PC_$task,$parcellation" -oo "logs/log_collect_PC_$task,$parcellation" -w 'ended("job_PC_*")' matlab -singleCompThread -r "get_PC_estimates_MainExperiment($task,[],$parcellation)"
	done
done


# summarize all the FC results
for parcellation in 1 4
do
	for task in {1..8}
	do
		bsub -W 4:00 -J "job_summary_FC_$task,$parcellation" -oo "logs/log_summary_FC_$task,$parcellation" -w 'ended("job_collect_FC_*")' matlab -singleCompThread -r "summarize_FC_MainExperiment($task,[],$parcellation)"
		bsub -W 4:00 -J "job_summary_PC_$task,$parcellation" -oo "logs/log_summary_PC_$task,$parcellation" -w 'ended("job_collect_PC_*")' matlab -singleCompThread -r "summarize_PC_MainExperiment($task,[],$parcellation)"
	done
done


# compute the within-subject similarity
for parcellation in 1 4
do
	bsub -W 4:00 -J "job_similarity_FC_$parcellation" -oo "logs/log_simmilarity_FC_$parcellation" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "similarity_FC_MainExperiment(1,[],[],$parcellation)"
	bsub -W 4:00 -J "job_similarity_PC_$parcellation" -oo "logs/log_simmilarity_PC_$parcellation" -w 'ended("job_summary_PC_*")' matlab -singleCompThread -r "similarity_FC_MainExperiment(2,[],[],$parcellation)"
done



# ----------------------------------------
# Comparison
# ----------------------------------------


# collect the group-level consistency
bsub -W 4:00 -J "job_consistency" -oo "logs/log_consistency" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "collect_consistency_MainExperiment(1,[],[],[1 4],1)"
bsub -W 4:00 -J "job_consistency_srDCM" -oo "logs/log_consistency_srDCM" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "collect_consistency_MainExperiment(2,[],[],[1 4],1)"


# summary files of rDCM and FC comparison
for parcellation in 1 4
do
	bsub -W 4:00 -J "job_compare_$parcellation" -oo "logs/log_compare_$parcellation" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "compare_ICC_MainExperiment(1,[],[],$parcellation)"
	bsub -W 4:00 -J "job_compare_srDCM_$parcellation" -oo "logs/log_compare_srDCM_$parcellation" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "compare_ICC_MainExperiment(2,[],[],$parcellation)"
done


# correlation between connection strength and relibaility
bsub -W 4:00 -J "job_consistency" -oo "logs/log_corr" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "collect_correlation_rDCM_MainExperiment(1,1,[],[],[1 4])"
bsub -W 4:00 -J "job_consistency_srDCM" -oo "logs/log_corr_srDCM" -w 'ended("job_summary_FC_*")' matlab -singleCompThread -r "collect_correlation_rDCM_MainExperiment(1,2,[],[],[1 4])"

