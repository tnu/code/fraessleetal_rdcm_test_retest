function similarity_FC_MainExperiment(variant,task,acq,parcellation_type)
% Takes the functional connectivity estimates (Pearson's correlation) from
% the resting-state and task-based fMRI data of the HCP dataset and then
% computes the within-subject similarity between test and retest (i.e.,
% re-identifying the same subject).
%
% Input:
%   variant             -- (1) fixed, (2) sparsity
%   task                -- which task is analyzed
%   acq                 -- which acquisition is analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Schaefer 2018
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'ICC')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cifti-matlab-master')))


% set the parcellation name
parcellations   = {'Glasser2016','AAL','Brainnetome2016','Schaefer2018'};

% folder for different rDCM variants
if ( variant == 1 )
	fc_folder = 'pearsonCorr';
elseif ( variant == 2 )
    fc_folder = 'partialCorr';
end


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_fc',parcellations{parcellation_type},fc_folder);


% all different tasks
task_name = {'rfMRI_REST','tfMRI_EMOTION','tfMRI_GAMBLING',...
    'tfMRI_LANGUAGE','tfMRI_MOTOR','tfMRI_RELATIONAL','tfMRI_SOCIAL',...
    'tfMRI_WM'};

% task names for title
task_title = {'REST','EMOTION','GAMBLING','LANGUAGE','MOTOR','RELATIONAL',...
    'SOCIAL','WM'};


% which tasks to analyze
if ( isempty(task) )
    task_analyze = 1:length(task_name);
else
    task_analyze = task;
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'figures',parcellations{parcellation_type},fc_folder);

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,'Similarity_All_comb_Atlas.pdf'),'file') )
        delete(fullfile(figure_folder,'Similarity_All_comb_Atlas.pdf'))
    end
end


% iterate over tasks
for task = task_analyze

    % search for the filename
    if ( isempty(acq) )
        filename = dir(fullfile(foldername,[task_name{task} '_comb_*']));
        filename = filename(1).name;
    else
        filename = [dir(fullfile(foldername,[task_name{task} '1*'])); dir(fullfile(foldername_rDCM,[task_name{task} '2*']))];
        filename = filename(acq).name;
    end


    % load the results array
    temp    = load(fullfile(foldername,filename));
    results = temp.results;


    % asign the data
    if ( variant == 1 )
        fullCorr_allSubjects = results.fullCorr_AllSubjects;
    elseif ( variant == 2 )
        fullCorr_allSubjects = results.partialCorrReg_AllSubjects;
    end
    
    
    % get functional connectivity estimates
    for int = 1:size(fullCorr_allSubjects,1)
        for int2 = 1:size(fullCorr_allSubjects,2)

            % subjects with data
            subject_vector = find(isfinite(fullCorr_allSubjects{int,int2,1}) & isfinite(fullCorr_allSubjects{int,int2,2}));

            % get the arrays for session 1 and 2
            fullCorr_allSubjects_S1(int,int2,:) = squeeze(fullCorr_allSubjects{int,int2,1}(subject_vector));
            fullCorr_allSubjects_S2(int,int2,:) = squeeze(fullCorr_allSubjects{int,int2,2}(subject_vector));

        end
    end
    
    % remove diagonal
    matrix = ones(size(fullCorr_allSubjects_S1,1),size(fullCorr_allSubjects_S1,2));
    matrix = matrix - eye(size(matrix));
    
    % restrict to respective connections
    for int3 = 1:size(fullCorr_allSubjects_S1,3)
        fullCorr_allSubjects_S1(:,:,int3) = fullCorr_allSubjects_S1(:,:,int3) .* matrix;
        fullCorr_allSubjects_S2(:,:,int3) = fullCorr_allSubjects_S2(:,:,int3) .* matrix;
    end
    

    % empty array
    CorrectFalse_allSubjects = zeros(size(fullCorr_allSubjects_S1,3),2);
    Identified_allSubjects   = zeros(size(fullCorr_allSubjects_S2,3),2);
    accuracy                 = NaN(1,2);
    
    % ordering of sessions
    for ord = 1:2
        
        % similarity matrix
        Similarity_allSubjects = NaN(size(fullCorr_allSubjects_S1,3),size(fullCorr_allSubjects_S2,3));
        
        % order of sessions
        switch ord
            case 1
                A_S1 = fullCorr_allSubjects_S1;
                A_S2 = fullCorr_allSubjects_S2;
            case 2
                A_S1 = fullCorr_allSubjects_S2;
                A_S2 = fullCorr_allSubjects_S1;
        end
        
        % compute similarity
        for subject_S1 = 1:size(A_S1,3)
            for subject_S2 = 1:size(A_S2,3)

                % get the arrays for both sessions
                temp_S1 = A_S1(:,:,subject_S1);
                temp_S2 = A_S2(:,:,subject_S2);

                % compute the correlation between the sessions
                sim_S1_S2 = corrcoef(temp_S1(matrix~=0),temp_S2(matrix~=0));

                % store the similarity values
                Similarity_allSubjects(subject_S1,subject_S2) = sim_S1_S2(1,2);

            end

            % get the subject that is closest
            [~,max_ind] = max(Similarity_allSubjects(subject_S1,:));
            Identified_allSubjects(subject_S1,ord) = max_ind;

            % check whether the same subject can be identified
            if ( max_ind == subject_S1)
                CorrectFalse_allSubjects(subject_S1,ord) = 1;
            end
        end
        
        % collate all results
        Identified_allSubjects_allTasks(subject_vector,task,ord) = Identified_allSubjects(:,ord);
    
        
        % define nice colormap
        [cbrewer_colormap] = cbrewer('seq', 'YlOrBr', 71, 'PCHIP');


        % compute accuracy
        accuracy(ord) = 100*sum(CorrectFalse_allSubjects(:,ord))/size(CorrectFalse_allSubjects,1);

        
        % plot average endogenous connectivity
        if ( ord == 1 )
            figure('units','normalized','outerposition',[0 0 1 1])
            imagesc(Similarity_allSubjects)
            hold on
            for id = 1:size(Similarity_allSubjects,1)
                rectangle('Position', [Identified_allSubjects(id)-0.5, id-0.5, 1, 1], 'EdgeColor', 'k', 'LineWidth', 1);
            end
            axis square
            colormap(cbrewer_colormap)
            set(gca,'xtick',[1 size(Similarity_allSubjects,2)])
            set(gca,'ytick',[1 size(Similarity_allSubjects,1)])
            ylabel('Subject ID (session 1)','FontSize',14)
            xlabel('Subject ID (session 2)','FontSize',14)
            title([task_title{task} ' | Similarity (ACC = ' num2str(round(accuracy(ord))) '%)'],'FontSize',18)
            print(gcf, '-dpdf', fullfile(figure_folder,['Similarity_All_comb_Atlas_temp' num2str(task)]), '-fillpage')
        end
    end
     
    % format specification of output
    formatSpec = '%17s | Accuracy (S1->S2): %4s%% (%2s/%2s) | Accuracy (S2->S1): %4s%% (%2s/%2s) \n';

    % print
    fprintf(formatSpec,task_title{task},num2str(round(accuracy(1),1)),...
        num2str(sum(CorrectFalse_allSubjects(:,1))),...
        num2str(size(CorrectFalse_allSubjects,1)),...
        num2str(round(accuracy(2),1)),...
        num2str(sum(CorrectFalse_allSubjects(:,2))),...
        num2str(size(CorrectFalse_allSubjects,1)))
    
    % clear all variables
    clear fullCorr_allSubjects_S1 fullCorr_allSubjects_S2
    
end

% line break
fprintf('\n')


% results array for majority vote
Identified_allSubjects_MV = NaN(size(Identified_allSubjects_allTasks,1),2,2);


% perform a majority vote
for ord = 1:2
    for subject = 1:size(Identified_allSubjects_allTasks,1)

        % remove zeros
        temp = Identified_allSubjects_allTasks(subject,:,ord);
        temp = temp(temp~=0);

        % find most common subject
        counter = zeros(1,size(Identified_allSubjects_allTasks,1));

        % count number of times subject is selected
        for int = 1:length(temp)
            for int2 = 1:size(Identified_allSubjects_allTasks,1)
                if ( temp(int) == int2 )
                    counter(int2) = counter(int2) + 1;
                end
            end
        end

        % find all maximal entries
        max_val = max(counter);
        subjects_all = find(counter == max_val);

        % store the selected subject
        if ( length(subjects_all) == 1 )
            Identified_allSubjects_MV(subject,1,ord) = subjects_all;
            Identified_allSubjects_MV(subject,2,ord) = subjects_all;
        else
            if ( sum(subjects_all == subject) ~= 0 )
                Identified_allSubjects_MV(subject,1,ord) = subject;
                Identified_allSubjects_MV(subject,2,ord) = subjects_all(find(subjects_all~=subject,1,'first'));
            else
                Identified_allSubjects_MV(subject,2,ord) = subjects_all(1);
                Identified_allSubjects_MV(subject,2,ord) = subjects_all(1);
            end
        end

        % check whether the same subject can be identified
        for int = 1:2
            if ( Identified_allSubjects_MV(subject,int,ord) == subject)
                CorrectFalse_allSubjects_MV(subject,int,ord) = 1;
            end
        end
    end
    
    % compute overall accuracy
    accuracy_MV(ord,:) = 100*sum(CorrectFalse_allSubjects_MV(:,:,ord))./size(CorrectFalse_allSubjects_MV,1);
    
end


% diplay for how many subjects sessions were most similar
fprintf(formatSpec,'Majority Vote (o)',num2str(round(accuracy_MV(1,1),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,1,1))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)),...
        num2str(round(accuracy_MV(2,1),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,1,2))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)))
    
fprintf(formatSpec,'Majority Vote (p)',num2str(round(accuracy_MV(1,2),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,2,1))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)),...
        num2str(round(accuracy_MV(2,2),1)),...
        num2str(sum(CorrectFalse_allSubjects_MV(:,2,2))),...
        num2str(size(CorrectFalse_allSubjects_MV,1)))
    

% append all PDFs
for task = task_analyze
    append_pdfs(fullfile(figure_folder,'Similarity_All_comb_Atlas.pdf'),...
        fullfile(figure_folder,['Similarity_All_comb_Atlas_temp' num2str(task) '.pdf']))
end
    
% delete the temporary files
delete(fullfile(figure_folder,'Similarity_All_comb_Atlas_temp*'))

end