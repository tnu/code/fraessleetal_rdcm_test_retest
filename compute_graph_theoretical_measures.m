function results = compute_graph_theoretical_measures(connectivity_matrix)
% Compute graph theoretical measures for an adjacency matrix.
% 
% This function uses the adjacency matrices and computes various graph-theoretical
% measures using the Brain Connectivity toolbox (BCT).
% 
% Input:
%   connectivity_matrix     -- directed and weighted connectivity matrix
% 
% Output:
%   results                 -- collection of graph theoretical measures
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% Add Brain Connectivity Toolbox
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'BCT')))


% if no connectivity matrix is given (subject excluded
if ( isempty(connectivity_matrix) )
    
    % asign empty graph theoretical measures
   	results.degree      = [];
    results.density     = [];
    results.cluster     = [];
	results.community   = [];
	results.structure   = [];
	results.distance    = [];
    results.efficiency  = [];
    results.centrality  = [];
    
    % stop funtion
    return
    
end


% normalize connection strengths
connectivity_matrix_norm = (abs(connectivity_matrix))./(max(abs(connectivity_matrix(:))));


% compute the in-degree and out-degree
[id,od,deg] = degrees_dir(connectivity_matrix);

% compute the strength
[is,os,str] = strengths_dir(connectivity_matrix);


% compute the density of network
[kden,~,~] = density_dir(connectivity_matrix);


% compute clustering coefficients (connections must be between 0 and 1)
% ccoef = clustering_coef_wd(connectivity_matrix_norm);
ccoef = deal(NaN);

% compute transivitivty (connections must be between 0 and 1)
% trans = transitivity_wd(connectivity_matrix_norm);
trans = deal(NaN);

% compute local efficiency (connections should be between 0 and 1)
% E_local = efficiency_wei(connectivity_matrix_norm, 2);
E_local = deal(NaN);


% compute the optimal community structure
% [M,Q] = community_louvain(connectivity_matrix,[],[],'negative_sym');
[M,Q] = deal(NaN);


% compute the assortativity coefficient
r_1 = NaN;%assortativity_wei(connectivity_matrix,1);
r_2 = NaN;%assortativity_wei(connectivity_matrix,2);
r_3 = NaN;%assortativity_wei(connectivity_matrix,3);
r_4 = NaN;%assortativity_wei(connectivity_matrix,4);

% compute the rich club coefficient
% [Rw] = rich_club_wd(connectivity_matrix);
[Rw] = deal(NaN);

% compute the core-periphery structure
[core, cstat] = core_periphery_dir(connectivity_matrix);


% compute the distance 
[SPL,hops,Pmat] = distance_wei_floyd(connectivity_matrix,'log');

% compute the characteristic path length
[lambda,efficiency,ecc,radius,diameter] = charpath(SPL);


% compute the mean first passage time
% MFPT = mean_first_passage_time(connectivity_matrix);
MFPT = NaN;

% compute the diffusion efficiency
% [GEdiff,Ediff] = diffusion_efficiency(connectivity_matrix);
[GEdiff,Ediff] = deal(NaN);


% compute the betweenness centrality
BC = betweenness_wei(SPL);

% compute the edge betweenness centrality
% [EBC,~] = edge_betweenness_wei(SPL);
EBC = NaN;


% store the results
results.degree.id           = id;
results.degree.od           = od;
results.degree.deg          = deg;

results.degree.is           = is;
results.degree.os           = os;
results.degree.str          = str;

results.density.kden        = kden;

results.cluster.ccoef       = ccoef;
results.cluster.trans       = trans;
results.cluster.E_local   	= E_local;

results.community.M         = M;
results.community.Q         = Q;
results.community.r_1       = r_1;
results.community.r_2       = r_2;
results.community.r_3       = r_3;
results.community.r_4       = r_4;

results.structure.Rw        = Rw;
results.structure.core      = core;
results.structure.cstat     = cstat;

results.distance.SPL        = SPL;
results.distance.hops       = hops;
results.distance.Pmat       = Pmat;
results.distance.lambda     = lambda;
results.distance.efficiency = efficiency;
results.distance.ecc        = ecc;
results.distance.radius     = radius;
results.distance.diameter   = diameter;

results.efficiency.MFPT     = MFPT;
results.efficiency.GEdiff   = GEdiff;
results.efficiency.Ediff    = Ediff;

results.centrality.BC       = BC;
results.centrality.EBC      = EBC;

end