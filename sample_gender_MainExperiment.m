function sample_gender_MainExperiment()
% Outputs the gender of the retest sample of HCP.
%
% Input:
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2021
% Copyright 2021 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% read the excel file
[~,~,RAW] = xlsread('HCP_Retest.xlsx');

% gender
HCP_gender = [0 0];


% asign the foldernames
foldername = FilenameInfo.Data_Path;


% get the subject list
Subject_List = dir(fullfile(foldername,'*'));
Subject_List = Subject_List(3:end);



% check whether participant is there
for s = 1:size(RAW,1)
    for s_targ = 1:length(Subject_List)
        if ( strcmp(num2str(RAW{s,1}),Subject_List(s_targ).name) )
            if ( strcmp(RAW{s,2},'F') )
                HCP_gender(1) = HCP_gender(1) + 1;
            else
                HCP_gender(2) = HCP_gender(2) + 1;
            end 
        end
    end
end

% output the results
fprintf('HCP Test-Retest: %d female, %d male \n',HCP_gender(1),HCP_gender(2))

end